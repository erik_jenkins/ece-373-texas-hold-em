#include "../includes/AI.h"


AI::AI(int money) : Player(money)
{
}

int AI::makeMove(int roundBet)
{
	int returnVal = 0;
	char option;
	if (roundBet == liveWallet - roundBet){
		option = 'f';
	}
	else if (roundBet >= wallet){
		option = 'f';
		
	}
	else{
		option = 'f';
	}

	int r = rand() % 100;

	switch (option){
	case 'f':
		setFolded(true);

		if (r < 5) returnVal = 1;
		else if (r < 50) returnVal = 2;
		else returnVal = 3;

		returnVal = returnVal * 10 + 1;
		return returnVal;

	case 'c':
		setFolded(false);

		if (r < 5) returnVal = 1;
		else if (r < 50) returnVal = 4;
		else returnVal = 3;

		returnVal = returnVal * 10 + 2;
		return returnVal;

	case 'k':
		bet(roundBet-getLiveWallet());
		setFolded(false);

		if (r < 5) returnVal = 1;
		else if (r < 50) returnVal = 2;
		else if (r < 75) returnVal = 5;
		else returnVal = 3;

		returnVal = returnVal * 10 + 3;
		return returnVal;

	case 'r':
	{
		double bett;
		bett = rand() % 100 + 1;
		bett = bett * (wallet - roundBet) / 100;
		bett += roundBet;
		bet((int)bett);

		if (r < 5) returnVal = 1;
		else if (r < 30) returnVal = 6;
		else if (r < 35) returnVal = 7;
		else returnVal = 3;

		returnVal = returnVal * 10 + 4;
		return returnVal;
	}
	case 'a':
	{
		bet(wallet);

		if (r < 5) returnVal = 1;
		else if (r < 30) returnVal = 2;
		else if (r < 60) returnVal = 8;
		else if (r < 75) returnVal = 4;
		else returnVal = 7;

		returnVal = returnVal * 10 + 5;
		return returnVal;
	}
	default:
		returnVal = 0;
		return returnVal;
	}
}
