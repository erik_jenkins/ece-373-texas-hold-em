#!/bin/bash
g++ -o HandTest HandDriver.cpp
./HandTest > TestResult.txt
count=`diff TestResult.txt HandExpected.txt | wc -w`
if [ $count -eq "0" ]; then
	echo Tests were successful. Yay!
else
	diff TestResult.txt HandExpected.txt
fi
rm HandTest TestResult.txt