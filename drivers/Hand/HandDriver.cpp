#include "../../source/Hand.cpp"
#include "../../source/Card.cpp"
#include <iostream>

int main() {
	Hand hand;

	hand.add(Card(Card::SPADES, Card::ACE));
	std::cout << hand.play(0).toString() << std::endl;
	std::cout << hand.getCards().size() << std::endl;

	hand.add(Card(Card::HEARTS, Card::TWO));
	std::cout << hand.play(1).toString() << std::endl;
	std::cout << hand.getCards().size() << std::endl;

	hand.popCard();
	std::cout << hand.getCards().size() << std::endl;

	hand.popCard();
	std::cout << hand.getCards().size() << std::endl;

	try {
		hand.popCard();
	} catch(emptyHandException* e) {
		std::cout << e->cwhat() << std::endl;
	}


}