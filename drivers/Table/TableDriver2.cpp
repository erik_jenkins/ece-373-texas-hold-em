#include "../../source/Table.cpp"
#include "../../source/Player.cpp"
#include "../../source/Card.cpp"
#include "../../source/Hand.cpp"
#include "../../source/Deck.cpp"
#include "../../source/AI.cpp"
#include <iostream>

int rank[] = { 14, 8, 13, 12, 8, 10, 11,//straightflush
14, 2, 14, 12, 14, 14, 6,//fourkind
11, 5, 11, 11, 14, 14, 6,//fh
12, 6, 12, 12, 6, 14, 6,//fh degen
5, 14, 8, 12, 10, 12, 4,//flush
5, 14, 8, 12, 10, 11, 4,//flush degen
3, 2, 14, 5, 10, 4, 6,//straight
3, 2, 7, 5, 14, 4, 6,//straight degen
13, 4, 13, 12, 13, 14, 6,//three
4, 2, 5, 5, 4, 7, 6,//two
2, 2, 8, 12, 4, 3, 11,//pair
14, 2, 5, 12, 7, 4, 6 };//high
int suit[] = { 0, 3, 0, 0, 1, 0, 0,//straightflush
0, 3, 1, 0, 3, 2, 0,//fourkind
0, 3, 2, 1, 1, 2, 0,//fh
0, 3, 2, 1, 1, 3, 0,//fh degen
0, 0, 0, 2, 1, 0, 0,//flush
0, 0, 0, 0, 0, 0, 0,//flush degen
0, 3, 3, 0, 1, 0, 0,//straight
0, 3, 3, 0, 1, 0, 0,//straight degen
0, 3, 2, 0, 1, 0, 0,//three
0, 1, 0, 2, 3, 3, 0,//two
0, 3, 2, 3, 1, 0, 0,//pair
0, 3, 2, 0, 1, 0, 1 };//high

int main(){
	Deck d;
	Player p1(10);
	Player p2(1000);
	Player p3(1000);
	Player p4(1000);
	Player p5(60);
	Player p6(1000);
	Table t;
	p1.bet(10);
	p2.bet(25);
	p3.bet(50);
	p4.bet(100);
	p5.bet(60);
	p3.setFolded(true);
	p2.setFolded(true);
	t.setPlayer(p1);
	t.setPlayer(p2);
	t.setPlayer(p3);
	t.setPlayer(p4);
	t.setPlayer(p5);
	t.setPlayer(p6);
	std::vector<int> temp = t.potCalc(3);
	while (!temp.empty()){
		std::cout << temp[0] << "\n";
		temp.erase(temp.begin());
	}
}