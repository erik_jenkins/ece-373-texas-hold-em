#include "../../source/Table.cpp"
#include "../../source/Player.cpp"
#include "../../source/Card.cpp"
#include "../../source/Hand.cpp"
#include "../../source/Deck.cpp"
//#include "../../source/AI.cpp"
#include <iostream>


int main(){
	std::srand((unsigned int)time(NULL));
	
//----------Testing simple methods for validity----------//

	//Instantiates table object
	Table table;
	
	//Adds 4 Players, alternating between (user) players and (agent) players	
	table.addPlayer(true);
	table.addPlayer(false);
	table.addPlayer(true);
	table.addPlayer(false);

	//Returns current bet amount -- should return 0
	std::cout << "Current Bet Amount: " << table.getAmount() << std::endl;
	
	//Modifies and returns current bet amount -- should be 12
	table.setAmount(500);
	std::cout << "New Bet Amount: " << table.getAmount() << std::endl;
	
	//Returns round count -- should return 0
	std::cout << "Current Round: " << table.getRound() << std::endl;
	
	//Modifies and returns new round count -- should be 7
	table.setRound(7);
	std::cout << "New Round: " << table.getRound() << std::endl;
	
	//Current participants -- should return 4
	std::cout << "Current Players: " << table.getSeats() << std::endl;
	
	//Removes 2 players and displays new participants -- should return 2
	table.removePlayer(0);
	table.removePlayer(1);
	std::cout << "New Players: " << table.getSeats() << std::endl;
	
//----------Begin Testing of More Advanced/Complex Functions----------//

	//Table t2(10000, 2, 100); //instantiates new table with initial wallet values of $10k and 2 seats
	
	
	
}
