#include "../../source/Card.cpp"
#include <iostream>
#include <typeinfo>

int main() {
	Card card1(Card::SPADES, Card::ACE);
	std::cout << card1.toString() << std::endl;
	std::cout << typeid(card1).name() << std::endl;
}