#pragma once

#include "../../source/Player.cpp"
#include "../../source/Hand.cpp"
#include "../../source/Card.cpp"
#include "../../source/Deck.cpp"
#include <stdio.h>

int rank[] = { 14, 8, 13, 12, 8, 10, 11,//straightflush
				14,	2,	14,	12,	14,	14,	6,//fourkind
				11,	5,	11,	11,	14,	14,	6,//fh
				12,	6,	12,	12,	6,	14,	6,//fh degen
				5,	14,	8,	12,	10,	12,	4,//flush
				5,	14,	8,	12,	10,	11,	4,//flush degen
				3,	2,	14,	5,	10,	4,	6,//straight
				3,	2,	7,	5,	14,	4,	6,//straight degen
				13,	4,	13,	12,	13,	14,	6,//three
				4,	2,	5,	5,	4,	7,	6,//two
				2,	2,	8,	12,	4,	3,	11,//pair
				14,	2,	5,	12,	7,	4,	6 };//high
int suit[] = { 0, 3, 0, 0, 1, 0, 0,//straightflush
				0,	3,	1,	0,	3,	2,	0,//fourkind
				0,	3,	2,	1,	1,	2,	0,//fh
				0,	3,	2,	1,	1,	3,	0,//fh degen
				0,	0,	0,	2,	1,	0,	0,//flush
				0,	0,	0,	0,	0,	0,	0,//flush degen
				0,	3,	3,	0,	1,	0,	0,//straight
				0,	3,	3,	0,	1,	0,	0,//straight degen
				0,	3,	2,	0,	1,	0,	0,//three
				0,	1,	0,	2,	3,	3,	0,//two
				0,	3,	2,	3,	1,	0,	0,//pair
				0,	3,	2,	0,	1,	0,	1 };//high

int main(){
	Deck d;
	Player p(100);
	for (int i = 0; i < 12; i++){
		std::vector<Card> c;
		p.addCard(d.get((rank[i * 7] - 2) + (suit[i * 7] * 13)));
		p.addCard(d.get((rank[i * 7 + 1] - 2) + (suit[i * 7 + 1] * 13)));
		c.push_back(d.get((rank[i * 7 + 2] - 2) + (suit[i * 7 + 2] * 13)));
		c.push_back(d.get((rank[i * 7 + 3] - 2) + (suit[i * 7 + 3] * 13)));
		c.push_back(d.get((rank[i * 7 + 4] - 2) + (suit[i * 7 + 4] * 13)));
		c.push_back(d.get((rank[i * 7 + 5] - 2) + (suit[i * 7 + 5] * 13)));
		c.push_back(d.get((rank[i * 7 + 6] - 2) + (suit[i * 7 + 6] * 13)));
		p.updateBuffer(c);
		p.calcHandRank();
		std::cout << p.getTieBreaker() << " " << p.getTieBreaker() << " " << p.getTieBreaker() << " " << p.getTieBreaker() << " " << p.getTieBreaker() << " " << "\n";
		p.newRound();
	}
}