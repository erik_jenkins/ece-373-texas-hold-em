#!/bin/bash
g++ -o PlayerTest PlayerDriver.cpp
./PlayerTest > PlayerTest.txt
count=`diff PlayerTest.txt PlayerExpected.txt | wc -w`
if [ $count -eq "0" ]; then
	echo Tests for Player were successful. Yay!
else
	diff PlayerTest.txt PlayerExpected.txt
fi
