#include "../../source/Deck.cpp"
#include "../../source/Card.cpp"
#include <iostream>


int main(){
	std::srand(time(NULL));
	Deck deck;
	deck.shuffle();
	for (int i = 0; i < 52; i++){
		std::cout << deck.draw().toString() << "\n";
	}
}