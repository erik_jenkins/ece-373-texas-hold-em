#!/bin/bash
g++ -o DeckTest DeckDriver.cpp
./DeckTest > TestResult.txt
count=`diff TestResult.txt DeckExpected.txt | wc -w`
if [ $count -eq "0" ]; then
	echo Tests were successful. Yay!
else
	diff TestResult.txt DeckExpected.txt
fi
rm DeckTest TestResult.txt