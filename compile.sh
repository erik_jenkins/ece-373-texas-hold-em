#!/bin/bash
# ECE 373: Compilation Script

CC="g++"
DEPS="/includes/"
CFLAGS="-c -Wall -pthread -lpthread"
LDFLAGS="-lncursesw"
SERVERINCLUDES="./source/AI.cpp ./source/Card.cpp ./source/Deck.cpp ./source/Hand.cpp ./source/Player.cpp ./source/Table.cpp"
CLIENTINCLUDES="./source/AI.cpp ./source/Card.cpp ./source/Deck.cpp ./source/Display.cpp ./source/Hand.cpp ./source/Player.cpp ./source/Table.cpp ./source/View.cpp"

g++ -c -I./includes ./source/Server.cpp -o bin/Server.o
g++ -o ./bin/Server $SERVERINCLUDES ./bin/Server.o ./lib/libxmlrpc_server_abyss++.a ./lib/libxmlrpc_server++.a ./lib/libxmlrpc_server_abyss.a ./lib/libxmlrpc_server.a ./lib/libxmlrpc_abyss.a  -lpthread ./lib/libxmlrpc++.a ./lib/libxmlrpc.a ./lib/libxmlrpc_util.a ./lib/libxmlrpc_xmlparse.a ./lib/libxmlrpc_xmltok.a

g++ -c -I./includes ./source/Client.cpp -o bin/Client.o
g++ -o ./bin/Client $CLIENTINCLUDES ./bin/Client.o ./lib/libxmlrpc_client++.a ./lib/libxmlrpc_client.a ./lib/libxmlrpc++.a ./lib/libxmlrpc.a ./lib/libxmlrpc_util.a ./lib/libxmlrpc_xmlparse.a ./lib/libxmlrpc_xmltok.a  -L/usr/lib/i386-linux-gnu -lcurl ./lib/libxmlrpc_packetsocket.a $LDFLAGS

rm -f bin/*.o