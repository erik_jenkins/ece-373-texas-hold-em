#include "../includes/Server.h"

Table t;

static void detectResize(int sig);
void printResize(void);
void betPhase();
void winPhase();
void produce(int state, int who);
void produceString(std::string message, int who);
void producePots(std::vector<int> pots, int who);
int consume();

std::string drawBalances();
int firstBetter;
int playersCount;
int foldCount;
int startup;

/*--------------------------------
		RPC LAND
--------------------------------*/

int viewState[8];
int viewConsume = -10;
std::string viewString = "";
std::vector<int> viewPots;

/*class sampleAddMethod : public xmlrpc_c::method {
public:
	sampleAddMethod() {
		// signature and help strings are documentation -- the client
		// can query this information with a system.methodSignature and
		// system.methodHelp RPC.
		this->_signature = "i:ii";
		// method's result and two arguments are integers
		this->_help = "This method adds two integers together";
	}
	void
		execute(xmlrpc_c::paramList const& paramList,
		xmlrpc_c::value * const retvalP) {
			int const addend(paramList.getInt(0));
			int const adder(paramList.getInt(1));
			paramList.verifyEnd(2);
			*retvalP = xmlrpc_c::value_int(addend + adder);
			// Sometimes, make it look hard (so client can see what it's like
			// to do an RPC that takes a while).
			if (adder == 1)
				sleep(2);
	}
};*/

class startMain : public xmlrpc_c::method {
public:
	startMain() {};

	void execute(xmlrpc_c::paramList const& paramList,
		xmlrpc_c::value * const retvalP)
	{
		startup = 1;
		*retvalP = xmlrpc_c::value_int(42);
	}
};

class rpcConsume : public xmlrpc_c::method {
public:
	rpcConsume() {};

	void execute(xmlrpc_c::paramList const& paramList,
		xmlrpc_c::value * const retvalP)
	{
		*retvalP = xmlrpc_c::value_int(viewState[0]);
		viewState[0] = -1;
	}
};

class checkReady : public xmlrpc_c::method {
public:
	checkReady() {};

	void execute(xmlrpc_c::paramList const& paramList,
		xmlrpc_c::value * const retvalP)
	{
		*retvalP = xmlrpc_c::value_int(viewState[0]);
	}
};

class receiveCommand : public xmlrpc_c::method {
public:
	receiveCommand() {
		this->_signature = "i:i";
	};

	void execute(xmlrpc_c::paramList const& paramList,
		xmlrpc_c::value * const retvalP)
	{
		int const betAmount = paramList.getInt(0);
		viewConsume = betAmount;
		*retvalP = xmlrpc_c::value_int(42);
	}
};

class getString : public xmlrpc_c::method {
public:
	getString() {};

	void execute(xmlrpc_c::paramList const& paramList,
		xmlrpc_c::value * const retvalP)
	{
		*retvalP = xmlrpc_c::value_string(viewString);
		viewState[0] = -1;
	}
};

class drawAllHands : public xmlrpc_c::method {
public:
	drawAllHands() {};

	void execute(xmlrpc_c::paramList const& paramList,
		xmlrpc_c::value* const retvalP)
	{
		std::vector<xmlrpc_c::value> arrayData;
		for (int j = 0; j < 2; j++){
			for (int i = 0; i < t.getSeats(); i++){
				if (!(t.getPlayer(i)->hasFolded())){
					int card = t.getPlayer(i)->getHand().getCards()[j].serialize();
					arrayData.push_back(xmlrpc_c::value_int(t.getPlayer(i)->getID() * 100 + card));
				}
			}
		}
		xmlrpc_c::value_array array1(arrayData);
		*retvalP = array1;
		viewState[0] = -1;
	}
};

class drawCommunityCards : public xmlrpc_c::method {
public:
	drawCommunityCards() {};

	void execute(xmlrpc_c::paramList const& paramList,
		xmlrpc_c::value * const retvalP)
	{
		std::vector<xmlrpc_c::value> arrayData;
		std::cout << "Cards on table: " << t.getCardsOnTable().size() << "\n";
		for (int i = 0; i < t.getCardsOnTable().size(); i++)
		{
			arrayData.push_back(xmlrpc_c::value_int(t.getCardsOnTable()[i].serialize()));
		}
		xmlrpc_c::value_array array1(arrayData);
		*retvalP = array1;
		viewState[0] = -1;
	}
};

class drawPots : public xmlrpc_c::method {
public:
	drawPots() {};

	void execute(xmlrpc_c::paramList const& paramList,
		xmlrpc_c::value * const retvalP)
	{
		std::vector<xmlrpc_c::value> arrayData;

		for (int i = 0; i < viewPots.size(); i++)
		{
			arrayData.push_back(xmlrpc_c::value_int(viewPots[i]));
		}

		*retvalP = xmlrpc_c::value_array(arrayData);
		viewState[0] = -1;
	}
};

void* rpcRun(void*){
	try {
		xmlrpc_c::registry myRegistry;

		xmlrpc_c::methodPtr const startMainP(new startMain);
		xmlrpc_c::methodPtr const rpcConsumeP(new rpcConsume);
		xmlrpc_c::methodPtr const checkReadyP(new checkReady);
		xmlrpc_c::methodPtr const receiveCommandP(new receiveCommand);
		xmlrpc_c::methodPtr const getStringP(new getString);
		xmlrpc_c::methodPtr const drawAllHandsP(new drawAllHands);
		xmlrpc_c::methodPtr const drawCommunityCardsP(new drawCommunityCards);
		xmlrpc_c::methodPtr const drawPotsP(new drawPots);

		myRegistry.addMethod("startMain", startMainP);
		myRegistry.addMethod("rpcConsume", rpcConsumeP);
		myRegistry.addMethod("checkReady", checkReadyP);
		myRegistry.addMethod("sendParam", receiveCommandP);
		myRegistry.addMethod("getString", getStringP);
		myRegistry.addMethod("drawAllHands", drawAllHandsP);
		myRegistry.addMethod("drawCommunityCards", drawCommunityCardsP);
		myRegistry.addMethod("drawPots", drawPotsP);

		xmlrpc_c::serverAbyss myAbyssServer(
			xmlrpc_c::serverAbyss::constrOpt()
			.registryP(&myRegistry)
			.portNumber(8080));

		myAbyssServer.run();
		// xmlrpc_c::serverAbyss.run() never returns
		assert(false);
	}
	catch (std::exception const& e) {
		std::cerr << "Something failed. " << e.what() << std::endl;
	}
}

/*--------------------------------
		END RPC LAND
		START MAIN
--------------------------------*/

int main(int argc, char* argv[])
{
	startup = 0;
	for (int i = 0; i < 8; i++) viewState[i] = -1;

	pthread_t rpcDo;
	pthread_create(&rpcDo, NULL, rpcRun, NULL);

	std::srand((unsigned int)time(NULL));

	// initialize view and draw the main menu
	while (startup == 0);
	// add human player to table and set ID
	t.addPlayer(true);
	t.getPlayer(0)->setID(0);

	//Fill in empty seats with agents
	for (int i = 1; i < 8; i++)
	{
		t.addPlayer(false);
		t.getPlayer(i)->setID(i);
	}
	std::cout << "Game Has Started\n";
	while (t.getSeats()>1){

		t.dealCards(); //deals cards to players

		//v.drawTopBanner(drawBalances());
		produce(1, -1);
		std::cout << "Drawing Balances\n";
		produceString(drawBalances(), -1);
		
		//v.drawAllHands(t, true, true);
		produce(2, -1);
		produce(0, -1);
		produce(11, -1);

		t.getPlayer((t.getDealer() + 2) % t.getSeats())->bet(t.getBigBlind());
		t.getPlayer((t.getDealer() + 1) % t.getSeats())->bet(t.getBigBlind() / 2);

		t.setAmount(t.getBigBlind());

		//v.drawTopBanner(drawBalances());
		produce(1, -1);
		produceString(drawBalances(), -1);

		playersCount = t.getSeats();
		foldCount = t.getSeats();

		firstBetter = ((t.getDealer() + 3) % t.getSeats());
		betPhase();
		firstBetter = ((t.getDealer() + 1) % t.getSeats());

		t.burn();

		for (int i = 0; i < 3; i++){
			t.drawToTable();

			if (foldCount > 1){
				sleep(1);
				//v.drawCommunityCards(t.getCardsOnTable());
				produce(3, -1);
				std::cout << "Drawing Commcards\n";
				produce(1, -1);
				std::cout << "Commcards done\n";
			}
		}

		if (playersCount > 1) betPhase();

		t.burn();
		t.drawToTable();
		if (foldCount > 1){
			//v.drawCommunityCards(t.getCardsOnTable());
			produce(3, -1);
			produce(1, -1);
		}

		if (playersCount > 1) betPhase();

		t.burn();
		t.drawToTable();
		if (foldCount > 1){
			//v.drawCommunityCards(t.getCardsOnTable());
			produce(3, -1);
			produce(1, -1);
		}

		if (playersCount > 1) betPhase();

		if (foldCount > 1){
			//v.drawCommunityCards(t.getCardsOnTable());
			produce(3, -1);
			produce(1, -1);
			//v.drawAllHands(t, false, false);
			produce(2, -1);
			produce(0, -1);
			produce(00, -1);
		}

		//v.resetMessages();
		produce(4, -1);
		//v.clearMessages();
		produce(5, -1);
		std::cout << "Starting Winphase\n";
		winPhase();

		//v.drawBottomBanner("Press a Key to Start a New Round");
		produce(6, -1);
		produceString("Press a Key to Start a New Round", -1);
		//v.promptLoop();
		produce(7, -1);
		produce(1, -1);
		//v.resetMessages();
		produce(4, -1);
		//v.clearScreen();
		produce(8, -1);

		////Resets the deck and sets up a new round
		if (t.newRound(0) == 1){
			//v.drawTopBanner("You Have Been Eliminated Exit Press a Key to Exit");
			produce(1, 0);
			//v.promptLoop();
			produce(7, -1);
			produce(1, 0);
		}
		//v.drawTopBanner(drawBalances());
		produce(1, -1);
		produceString(drawBalances(), -1);
	}
}

/*--------------------------------
Helper Functions
--------------------------------*/

void betPhase()
{
	//Sets the correct betting order based on the "dealer"
	int currentPlayer = firstBetter;
	int lastPlayer = t.getSeats() + currentPlayer - 1;

	//Basically a loop to check if everyone has met the bet
	//i.e there are no outstanding bets
	while (currentPlayer <= lastPlayer)
	{
		//If everyone has folded during this betting phase break the loop
		if (foldCount == 0) break;

		//A conversion from an unbounded frame to a bounded frame
		int currentIndex = currentPlayer % t.getSeats();

		//A check to make sure the player in question is eligible to bet
		if (!(t.getPlayer(currentIndex)->hasFolded()) && (t.getPlayer(currentIndex)->getWallet() != 0)){
			//Start of the AI betting logic a compressed for of player logic
			if (t.getPlayer(currentIndex)->isAI()){
				usleep(500000);
				//If the human player has foled "speed up" the drawing process
				if (!t.getPlayer(0)->hasFolded()) sleep(rand() % 2);
				//A temporary value to see how much the AI has raised
				int walletdiff = t.getPlayer(currentIndex)->getWallet();
				//Tell the AI to make their move and stores the return value which is an encryption
				//of what the AI has said and done
				int AIAll = ((AI*)t.getPlayer(currentIndex))->makeMove(t.getAmount());
				//Sifts out the first digit of the return value which is the AI's move
				int AIMove = AIAll % 10;
				//Sifts out the second digit of the return value which is the AI's flavor message
				int AISay = AIAll / 10;
				//If the AI has raised the current bet
				if (AIMove > 3){
					//Update the betting loop to indicate a player has bet
					lastPlayer = (currentIndex + t.getSeats() - 2);
					currentPlayer = currentIndex;

					int temp = t.getAmount();
					//If the player has effectively gone All-In reduce the total player's eligible
					//for betting
					if (t.getPlayer(currentIndex)->getWallet() == 0) playersCount--;
					//Update the table's current bet amount
					t.setAmount(temp + walletdiff - t.getPlayer(currentIndex)->getWallet());
				}
				//If the AI has folded
				if (AIMove == 1){
					//Remove his displayed hand for clarity
					//v.clearHand(t.getPlayer(currentIndex)->getID());
					produce(11, -1);
					produce(t.getPlayer(currentIndex)->getID(), -1);

					t.getPlayer(currentIndex)->setFolded(true);
					//Update the player and fold counts
					playersCount--;
					foldCount--;
				}

				//Retrive the text that the AI has "said" and display it
				std::stringstream AIMessages;
				std::vector<std::string> messageVector;
				messageVector = ((AI*)t.getPlayer(currentIndex))->getMessage(AIAll, walletdiff);
				for (int i = 0; i < messageVector.size(); i++){
					//v.updateMessage(messageVector[i]);
					produce(9, -1);
					produceString(messageVector[i], -1);
					//v.printMessages();
					produce(10, -1);
				}
			}

			//Begin the human betting logic
			else{
				int option;
				//Checks to see the avalible options based on bet amounts, etc. and
				//indicate it to the veiw

				//Check, Bet, All-In, Fold
				if (t.getAmount() == t.getPlayer(currentIndex)->getLiveWallet()){
					//option = v.promptPlayer(0, t.getAmount());
					produce(12, t.getPlayer(currentIndex)->getID());
					produce(t.getAmount(), t.getPlayer(currentIndex)->getID());
					option = consume();
				}
				//All-In, Fold
				else if (t.getAmount() >= t.getPlayer(currentIndex)->getWallet()){
					//option = v.promptPlayer(1, 0);
					produce(15, t.getPlayer(currentIndex)->getID());
					produce(0, t.getPlayer(currentIndex)->getID());
					option = consume();
				}
				//Call, Bet, All-In, Fold
				else{
					//option = v.promptPlayer(2, t.getAmount() - t.getPlayer(currentIndex)->getLiveWallet());
					produce(16, t.getPlayer(currentIndex)->getID());
					produce(t.getAmount() - t.getPlayer(currentIndex)->getLiveWallet(), t.getPlayer(currentIndex)->getID());
					option = consume();
				}
				//Input Gathering is done now start the processing of it
				//v.clearControls();
				produce(13, t.getPlayer(currentIndex)->getID());
				//Fold - takes player out of round
				if (option == -1) {
					t.getPlayer(currentIndex)->setFolded(true);
					//v.clearHand(t.getPlayer(currentIndex)->getID());
					produce(11, -1);
					produce(t.getPlayer(currentIndex)->getID(), -1);
					playersCount--;
					foldCount--;
				}
				//Check - nothing happens
				else if (option == 0){
					t.getPlayer(currentIndex)->setFolded(false);
				}
				//Call - update players wallets
				else if (option == -2){
					t.getPlayer(currentIndex)->setFolded(false);
					t.getPlayer(currentIndex)->bet(t.getAmount() - t.getPlayer(currentIndex)->getLiveWallet());
				}
				//The user has made a bet
				//The option is the bet amount
				else if (option > 0){
					t.getPlayer(currentIndex)->setFolded(false);
					//Update the final better index
					lastPlayer = (currentIndex + t.getSeats() - 2);
					currentPlayer = currentIndex;
					//Process the Bet
					int temp;
					if (t.getAmount() == t.getPlayer(currentIndex)->getLiveWallet()){
						temp = t.getPlayer(currentIndex)->bet(option);
					}
					else temp = t.getPlayer(currentIndex)->bet(option + t.getAmount());
					t.setAmount(temp);
					//If the players bet was an All-In reduce the eligible players
					if (t.getPlayer(currentIndex)->getWallet() == 0) playersCount--;
				}
				//All-In
				else if (option == -3){
					t.getPlayer(currentIndex)->setFolded(false);
					//Update the final better index
					lastPlayer = (currentIndex + t.getSeats() - 2);
					currentPlayer = currentIndex;
					//Process the Bet
					int playerWallet = t.getPlayer(currentIndex)->getWallet();
					if (playerWallet > t.getAmount()) t.setAmount(playerWallet);
					t.getPlayer(currentIndex)->bet(playerWallet);
					playersCount--;
				}
				else{
				}
			}
			//v.drawTopBanner(drawBalances());
			produce(1, -1);
			produceString(drawBalances(), -1);
		}
		std::vector<int> temp = t.potCalc((lastPlayer + 1) % t.getSeats());
		//while (!temp.empty()){
		//	std::cout << temp[0] << "\n";
		//	temp.erase(temp.begin());
		//}
		//v.drawPots(temp);
		produce(14, -1);
		producePots(temp, -1);
		currentPlayer++;
	}
}



void winPhase(){

	std::vector<int> winVec = t.winMacro();//Determines winner(s) and distributes winnings

	for (unsigned int i = 0; i < winVec.size(); i++){
		std::vector<int> winPot;
		if (winVec[i] > 10000){
			winPot.push_back(winVec[i] % 10);
			winPot.push_back((winVec[i] / 10) % 10);
			winPot.push_back((winVec[i] / 100) % 10);
			winPot.push_back((winVec[i] / 1000) % 10);
		}
		else if (winVec[i] > 1000){
			winPot.push_back(winVec[i] % 10);
			winPot.push_back((winVec[i] / 10) % 10);
			winPot.push_back((winVec[i] / 100) % 10);
		}
		else if (winVec[i] > 100){
			winPot.push_back(winVec[i] % 10);
			winPot.push_back((winVec[i] / 10) % 10);
		}
		else{
			winPot.push_back(winVec[i] % 10);
		}
		std::stringstream winners;
		winners << "Winners for Pot " << i << " are: ";
		if (winPot.size() == 4){
			winners << "P" << t.getPlayer(winPot[0])->getID() << ", ";
			winners << "P" << t.getPlayer(winPot[1])->getID() << ", ";
			winners << "P" << t.getPlayer(winPot[2])->getID() << ", ";
			winners << "P" << t.getPlayer(winPot[3])->getID();
		}
		else if (winPot.size() == 3){
			winners << "P" << t.getPlayer(winPot[0])->getID() << ", ";
			winners << "P" << t.getPlayer(winPot[1])->getID() << ", ";
			winners << "P" << t.getPlayer(winPot[2])->getID();
		}
		else if (winPot.size() == 2){
			winners << "P" << t.getPlayer(winPot[0])->getID() << ", ";
			winners << "P" << t.getPlayer(winPot[1])->getID();
		}
		else{
			winners.str("");
			winners << "Winner for Pot " << i << " is: ";
			winners << "P" << t.getPlayer(winPot[0])->getID();
		}
		//v.updateMessage(winners.str());
		produce(9, -1);
		produceString(winners.str(), -1);
	}
	//v.updateMessage("");
	produce(9, -1);
	produceString(" ", -1);
	//v.printMessages();
	produce(10, -1);
}

std::string drawBalances()
{
	std::stringstream balances;
	balances << " Cash: " << t.getPlayer(0)->getWallet();
	for (int i = 1; i < t.getSeats(); i++){
		balances << " ";
		if (t.getPlayer(i)->isAI()) balances << "AI" << t.getPlayer(i)->getID() << ": ";
		else balances << "P" << t.getPlayer(i)->getID() << ": ";
		balances << t.getPlayer(i)->getWallet();
	}
	return balances.str();
}

//Produce and Consume
//We deal with the viewstate array and take in the state to set it to and who to set, -1 for all players
//After producing stall out until function all variables have been consumed
//Single Player-wise the who is always 0
void produce(int state, int who){
	//Produce Step
	if (who == -1){
		for (int i = 0; i < t.getSeats(); i++){
			if (!t.getPlayer(i)->isAI()){
				viewState[t.getPlayer(i)->getID()] = state;
			}
		}
	}
	else viewState[who] = state;

	//Wait on Consume state
	bool stillWait = true;
	while (stillWait){
		stillWait = false;
		for (int i = 0; i < 8; i++){
			if (viewState[i] > -1) stillWait = true;
		}
	}
}

void produceString(std::string message, int who){
	//Produce Step
	viewString = message;
	if (who == -1){
		for (int i = 0; i < t.getSeats(); i++){
			if (!t.getPlayer(i)->isAI()){
				viewState[t.getPlayer(i)->getID()] = 0;
			}
		}
	}
	else viewState[who] = 0;

	//Wait on Consume state
	bool stillWait = true;
	while (stillWait){
		stillWait = false;
		for (int i = 0; i < 8; i++){
			if (viewState[i] > -1) stillWait = true;
		}
	}
	viewString = "";
}

void producePots(std::vector<int> pots, int who)
{
	//Produce Step
	std::vector<int> viewPots;
	for (int i = 0; i < pots.size(); i++)
		viewPots.push_back(pots[i]);

	if (who == -1){
		for (int i = 0; i < t.getSeats(); i++){
			if (!t.getPlayer(i)->isAI()){
				viewState[t.getPlayer(i)->getID()] = 0;
			}
		}
	}
	else viewState[who] = 0;

	//Wait on Consume state
	bool stillWait = true;
	while (stillWait){
		stillWait = false;
		for (int i = 0; i < 8; i++){
			if (viewState[i] > -1) stillWait = true;
		}
	}

	while (viewPots.size()>0)
		viewPots.pop_back();

}

//We deal with our viewConsume variable
//We stall and wait until the view has updated(produced somthing) in to the viewConsume variable
//Once there has been somthing produced we reset the variable and return it
int consume() {
	// Sleep the main thread until the rpc thread changes viewConsume
	while (viewConsume == -10) usleep(1000);

	int temp = viewConsume;
	viewConsume = -10;
	return temp;
}

void detectResize(int sig)
{
	/*v.getDisplay()->handleResize(sig);
	printResize();
	signal(SIGWINCH, detectResize);*/
}

void printResize(void)
{
	//v.reDraw();
}
