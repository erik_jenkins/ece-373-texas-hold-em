#include "../includes/Hand.h"

Hand::Hand() {
	// default constructor
}

void Hand::add(Card card) {
	cards.push_back(card);
}

Card Hand::play(int cardInt) {
	return cards.at(cardInt);
}

void Hand::popCard(){
	if((int)cards.size() == 0) throw new emptyHandException;
	cards.pop_back();
}

void Hand::clearHand(){
	while (!cards.empty()) popCard();
}

std::vector<Card> Hand::getCards() {
	return cards;
}