#include "../includes/Client.h"

View v;

int main(int argc, char **) {
	if (argc - 1 > 0) {
		std::cerr << "This program has no arguments" << std::endl;
		exit(1);
	}
	try {
		xmlrpc_c::clientSimple myClient;

		std::string const serverUrl("http://localhost:8080/RPC2");
		
		xmlrpc_c::value result;

		v.drawMainMenu();
		myClient.call(serverUrl, "startMain", &result);
		// Assume the method returned an integer; throws error if not
		//Game Has Started
		while(1){
			xmlrpc_c::value whatDo;
			myClient.call(serverUrl, "rpcConsume", &whatDo);
			int const whatToDo = xmlrpc_c::value_int(whatDo);
			if (whatToDo > 0){
				switch (whatToDo){
				case 1://Draw Top Banner
				{
					//Since we're consuming a value we must have a ready check to see if the
					//mutex/value has been produced on the server yet
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "getString", &viewArgs);
					std::string const balances = xmlrpc_c::value_string(viewArgs);
					v.drawTopBanner(balances);
					break;
				}
				case 2://Draw All Hands
				{
					//Since we're consuming a value we must have a ready check to see if the
					//mutex/value has been produced on the server yet
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "drawAllHands", &viewArgs);
					xmlrpc_c::value_array viewArray(viewArgs);
					std::vector<xmlrpc_c::value> const handArray(viewArray.vectorValueValue());
					vector<int> hands;
					for (int i = 0; i < handArray.size(); i++){
						hands.push_back(xmlrpc_c::value_int(handArray[i]));
					}
					
					//Since we're consuming a value we must have a ready check to see if the
					//mutex/value has been produced on the server yet
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs1;
					myClient.call(serverUrl, "rpcConsume", &viewArgs1);
					int const delay_show = xmlrpc_c::value_int(viewArgs1);
					
					v.drawAllHands(hands, (delay_show/10 == 1), (delay_show%10 == 1), 0);
					break;
				}
				case 3://Draw Community Cards
				{
					//Since we're consuming a value we must have a ready check to see if the
					//mutex/value has been produced on the server yet
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "drawCommunityCards", &viewArgs);
					xmlrpc_c::value_array viewArray(viewArgs);
					std::vector<xmlrpc_c::value> const commArray(viewArray.vectorValueValue());
					vector<int> commCards;
					for (int i = 0; i < commArray.size(); i++){
						commCards.push_back(xmlrpc_c::value_int(commArray[i]));
					}
					v.drawCommunityCards(commCards);
					break;
				}
				case 4://Reset Messages
				{
					v.resetMessages();
					break;
				}
				case 5://Clear Messages
				{
					v.clearMessages();
					break;
				}
				case 6://Draw Bottom Banner
				{
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "getString", &viewArgs);
					std::string const balances = xmlrpc_c::value_string(viewArgs);
					v.drawBottomBanner(balances);
					break;
				}
				case 7://Prompt Loop
				{
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					v.promptLoop();
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "rpcConsume", &viewArgs);
					break;
				}
				case 8://Clear Screen
				{
					v.clearScreen();
					break;
				}
				case 9://Update Messages
				{
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "getString", &viewArgs);
					std::string const msg = xmlrpc_c::value_string(viewArgs);
					v.updateMessage(msg);
					break;
				}
				case 10://Print Messages
				{
					v.printMessages();
					break;
				}
				case 11://Clear Hand
				{
					//Since we're consuming a value we must have a ready check to see if the
					//mutex/value has been produced on the server yet
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "rpcConsume", &viewArgs);
					int const id = xmlrpc_c::value_int(viewArgs);
					v.clearHand(id);
					break;
				}
				case 12://Prompt Player 0
				{ //for all prompt player cases, client must know what information is being sent back to the server (raise, call, fold, etc)
					//Since we're consuming a value we must have a ready check to see if the
					//mutex/value has been produced on the server yet
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "rpcConsume", &viewArgs);
					int const plr = xmlrpc_c::value_int(viewArgs);					
					int option = v.promptPlayer(0, plr);

					xmlrpc_c::value viewArgs1;
					myClient.call(serverUrl, "sendParam", "i", &viewArgs1, option);

					break;
				}
				case 13://Clear Control
				{
					v.clearControls();
					break;
				}
				case 14://Draw Pots
				{
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "drawPots", &viewArgs);
					xmlrpc_c::value_array viewArray(viewArgs);
					std::vector<xmlrpc_c::value> const pots(viewArray.vectorValueValue());
					vector<int> sidePots;
					for (int i = 0; i < pots.size(); i++){
						sidePots.push_back(xmlrpc_c::value_int(pots[i]));
					}

					v.drawPots(sidePots);
					break;

				}
				case 15://Prompt Player 1
				{
					//Since we're consuming a value we must have a ready check to see if the
					//mutex/value has been produced on the server yet
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "rpcConsume", &viewArgs);
					int const plr = xmlrpc_c::value_int(viewArgs);	
					int option = v.promptPlayer(1, plr);
					xmlrpc_c::value viewArgs1;
					myClient.call(serverUrl, "sendParam", "i", &viewArgs1, option);

					break;

				}
				case 16://Prompt Player 2
				{
					//Since we're consuming a value we must have a ready check to see if the
					//mutex/value has been produced on the server yet
					while (1){
						xmlrpc_c::value ready;
						myClient.call(serverUrl, "checkReady", &ready);
						int const rCheck = xmlrpc_c::value_int(ready);
						if (rCheck > -1) break;
						usleep(1000);
					}
					xmlrpc_c::value viewArgs;
					myClient.call(serverUrl, "rpcConsume", &viewArgs);
					int const plr = xmlrpc_c::value_int(viewArgs);
					int option = v.promptPlayer(2, plr);

					xmlrpc_c::value viewArgs1;
					myClient.call(serverUrl, "sendParam", "i", &viewArgs1, option);

					break;

				}
				default: break;
				}
			}
			usleep(250000);
		}
	}
	catch (std::exception const& e) {
		std::cerr << "Client threw error: " << e.what() << std::endl;
	}
	catch (...) {
		std::cerr << "Client threw unexpected error." << std::endl;
	}
	return 0;
}
