#include "../includes/Player.h"

//Constructors
Player::Player(int money){
	wallet = money;
	liveWallet = 0;
	rank = 0;
	Fold_FLAG = false;
	AI_FLAG = false;

	for (int i = 0; i < 4; i++){ suitCount[i] = 0; }
	for (int i = 0; i < 13; i++){ cardCount[i] = 0; }
	for (int i = 0; i < 5; i++){ occurrenceCount[i] = 0; }

}
//EndConstructors


/*------------------------------
The Following Are Basic Get/Sets
------------------------------*/

int Player::getWallet(){
	return wallet;
}

void Player::increaseWallet(int pot){
	wallet+=pot;
}

int Player::getLiveWallet(){
	return liveWallet;
}

void Player::setLiveWallet(int seto){
	liveWallet = seto;
}

int Player::getID(){
	return ID;
}

void Player::setID(int id){
	ID = id;
}

bool Player::hasFolded(){
	return Fold_FLAG;
}

void Player::setFolded(bool f){
	Fold_FLAG = f;
}

void Player::setAI(){
	AI_FLAG = true;
}

bool Player::isAI(){
	return AI_FLAG;
}

//Hand Manipulation Functions
Hand Player::getHand(){
	return hand;
}

void Player::addCard(Card card){
	hand.add(card);
}

void Player::clearHand(){
	while(!hand.getCards().empty()){
		hand.popCard();
	}
}
//End of Hand Manipulation Functions


int Player::bet(int current){
	if (current > wallet){
		liveWallet += wallet;
		wallet = 0;
	}
	else{
	wallet-=current;
	liveWallet+=current;
	}
	return liveWallet;
}

/*------------------------------
	End of Basic Get/Sets
------------------------------*/

void Player::updateBuffer(std::vector<Card> communityCard){
	std::vector<Card> comtemp;
	for(int i=0;i<5;i++){
		comtemp.push_back(communityCard[i]);
	}
	communityCardBuffer = comtemp;
}

void Player::setRank(int  num){
	if(rank < num){rank = num;}
}

int Player::getHandRank(){
	return rank;
}

void Player::calcHandRank(){
	//Initailize the arrays to 0
	for (int i = 0; i < 4; i++){ suitCount[i] = 0; }
	for (int i = 0; i < 14; i++){ cardCount[i] = 0; }
	for (int i = 0; i < 5; i++){ occurrenceCount[i] = 0; }

	//Calculate the arrays for hand
	for(int i = 0;i < 2;i++){
		Card card = hand.play(i);
		suitCount[card.getSuit()]++;
		cardCount[card.getRank()-1]++;
		if (card.getRank() == 14) cardCount[0]++;
	}
	//Calculate the arrays for the community cards
	for(int i = 0;i < 5;i++){ 
		suitCount[communityCardBuffer[i].getSuit()]++;
		cardCount[communityCardBuffer[i].getRank()-1]++;
		if (communityCardBuffer[i].getRank() == 14) cardCount[0]++;
	}
	//Record occurrence based on number of times the value pops up in cardCount array
	for(int i = 0;i < 13;i++){
		occurrenceCount[cardCount[i]]++;
	}

	/*TABLE OF HAND VALUES
	STRAIGHT FLUSH 9
	FOUR OF A KIND 8
	FULL HOUSE 7
	FLUSH 6
	STRAIGHT 5
	THREE OF A KIND 4
	TWO PAIR 3
	ONE PAIR 2
	HIGH CARD 1
	*/

	if (straightCheck() && flushCheck()){
		setRank(9);
	}
	flushCheck();
	setRank(occurrenceCheck());
	setTieBreaker();
}

std::vector<int> Player::getTieBreaker(){
	return winHand;
}

void Player::newRound(){

	for (int i = 0; i < 4; i++){ suitCount[i] = 0; }
	for (int i = 0; i < 14; i++){ cardCount[i] = 0; }
	for (int i = 0; i < 5; i++){ occurrenceCount[i] = 0; }
	while (!winHand.empty()){ winHand.pop_back(); }
	while (!communityCardBuffer.empty()){ communityCardBuffer.pop_back(); }

	hand.clearHand();
	rank = 0;
	Fold_FLAG = false;
	liveWallet = 0;
}

/*------------------------------
		Helper Methods
------------------------------*/

bool Player::flushCheck(){
	for(int i = 0; i < 4; i++){
		if(suitCount[i] >= 5){
			setRank(6);
			return true;
		}
	}
	return false;
}

bool Player::straightCheck(){
	for(int i = 0; i < 10; i++){
		if(cardCount[i] >= 1
			&& cardCount[i+1] >= 1
			&& cardCount[i+2] >= 1
			&& cardCount[i+3] >= 1
			&& cardCount[i+4] >= 1){
			setRank(5);
			return true;
		}
	}
	return false;
}

int Player::occurrenceCheck(){
	if(occurrenceCount[4] == 1) return 8; //Four of a kind
	else if(occurrenceCount[3] >= 1){
		if (occurrenceCount[3] > 1) return 7;
		if(occurrenceCount[2] >= 1) return 7; //Full house
		return 4; //Three of a kind
	}
	else if (occurrenceCount[2] >= 2) return 3;//Two pair
	else if(occurrenceCount[2] == 1) return 2;//One pair
	else return 1;//high card
}

void Player::setTieBreaker(){
	switch (rank){
	case 9:{//straight flush
		int tempCount[14];
		for (int i = 0; i < 14; i++){ tempCount[i] = 0; }
		for (int i = 0; i < 4; i++){
			if (suitCount[i] >= 5){
				for (int j = 0; j < 5; j++){
					if (communityCardBuffer[j].getSuit() == i) tempCount[communityCardBuffer[j].getRank() - 1]++;
				}
				for (int j = 0; j < 2; j++){
					if (hand.play(j).getSuit() == i) tempCount[hand.play(j).getRank() - 1]++;
				}
				tempCount[0] = tempCount[13];
			}
		}
		for (int i = 0; i < 10; i++){
			if (tempCount[i] >= 1
				&& tempCount[i + 1] >= 1
				&& tempCount[i + 2] >= 1
				&& tempCount[i + 3] >= 1
				&& tempCount[i + 4] >= 1){
				for (int j = 0; j < 5; j++){ winHand.push_back(i + j); }
			}
		}
		break;
	}
	case 7:{//full house
		for (int i = 0; i < 14; i++){
			if (cardCount[i] == 2){
				winHand.push_back(i);
				winHand.push_back(i);
			}
		}
		for (int i = 0; i < 14; i++){
			if (cardCount[i] == 3){
				winHand.push_back(i);
				winHand.push_back(i);
				winHand.push_back(i);
			}
		}
		break;
	}
	case 6:{//flush
		int tempCount[14];
		for (int i = 0; i < 14; i++){ tempCount[i] = 0; }
		for (int i = 0; i < 4; i++){
			if (suitCount[i] >= 5){
				for (int j = 0; j < 5; j++){
					if (communityCardBuffer[j].getSuit() == i) tempCount[communityCardBuffer[j].getRank() - 1]++;
				}
				for (int j = 0; j < 2; j++){
					if (hand.play(j).getSuit() == i) tempCount[hand.play(j).getRank() - 1]++;
				}
			}
		}
		for (int i = 1; i < 14; i++){
			if (tempCount[i] > 0) winHand.push_back(i);
		}
		break;
	}
	case 5:{//straight
		for (int i = 0; i < 10; i++){
			if (cardCount[i] >= 1
				&& cardCount[i + 1] >= 1
				&& cardCount[i + 2] >= 1
				&& cardCount[i + 3] >= 1
				&& cardCount[i + 4] >= 1){
				for (int j = 0; j < 5; j++){ winHand.push_back(i + j); }
			}
		}
		break;
	}
	case 8://four of a kind
	case 4://three of a kind
	case 3://two pair
	case 2://one pair
	case 1:{//high card
		pairWin();
		break;
	}
	}
}

void Player::pairWin(){
	for (int i = 1; i < 14; i++){
		if (cardCount[i] == 1) winHand.push_back(i);
	}
	for (int i = 1; i < 14; i++){
		if (cardCount[i] > 1){
			for (int j = 0; j < cardCount[i]; j++){
				winHand.push_back(i);
			}
		}
	}
}