#include "../includes/Card.h"


const std::string suits[4] = { "spades", "clubs", "diamonds", "hearts" };
const std::string ranks[13] = { "two", "three", "four", "five", "six", \
"seven", "eight", "nine", "ten", "jack", "queen", "king", "ace" };

//Default Constructor
Card::Card(Card::SUIT suit, Card::RANK rank) {
	this->suit = suit;
	this->rank = rank;
}
//End of Constructors

void Card::setSuit(Card::SUIT suit) {
	this->suit = suit;
}

void Card::setRank(Card::RANK rank) {
	this->rank = rank;
}

Card::SUIT Card::getSuit() { return this->suit; }
Card::RANK Card::getRank() { return this->rank; }

int Card::serialize() {
	return(rank - 2 + suit * 13);
}

std::string Card::toString() {
	std::ostringstream os;
	os << ranks[getRank() - 2] << " of " << suits[getSuit()];
	return os.str();
}
