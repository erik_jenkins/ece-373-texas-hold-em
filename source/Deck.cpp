#include "../includes/Deck.h"


Deck::Deck() {
	// populate cards vector with all possible cards
	for (int suitInt = Card::SPADES; suitInt <= Card::HEARTS; suitInt++) {
		for (int rankInt = Card::TWO; rankInt <= Card::ACE; rankInt++) {
			Card::SUIT suit = static_cast<Card::SUIT>(suitInt);	// cast int to enum
			Card::RANK rank = static_cast<Card::RANK>(rankInt);

			Card newCard(suit, rank);
			cards.push_back(newCard);
		}
	}
}

static int randomizer (int i){
	return std::rand()%i;
}

int Deck::count() {
	return (int)cards.size();
}

Card Deck::draw() {
	if (count() == 0) throw new emptyDeckException;

	Card card = cards.back();
	cards.pop_back();
	return card;
}

Card Deck::get(int index){
	return cards[index];
}

void Deck::shuffle() {
	if (count() == 0) throw new emptyDeckException;
	std::random_shuffle(cards.begin(), cards.end(), randomizer);
}
