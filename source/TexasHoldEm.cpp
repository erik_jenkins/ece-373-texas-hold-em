#include "../includes/TexasHoldEm.h"

/*
ECE 373 - Software Intensive Engineering - Project 2
Class: Control
Description: Calls objects and subroutines in the propper order in order to play a game of Texas Hold 'Em.

Required Classes: Everything
-Array of Players
-Deck (and Cards)
-Table
-Hands
-Game (Session - in preparation for distribution)

State Logic / Process:
-Instantiate all necessary classes/objects
- [Leave space for the game to become multiplayer]
- Once everything is instantiated and declared, begin game and close session from outside players

- Initiate blind bet
- Deck deals 2 cards per player (rotating)
- Initiate Bet #1
- Burn top card from deck
- Deck draws 3 comm cards to the table
- Initiate Bet #2
- Burn top card from deck
- Deck draws 1 comm card
- Initiate Bet #3
- Burn top card
- Deck Draws 1 comm card
- Initiate Bet #4

- Get value of each player's hand and compare against comm cards
- Highest value wins the pot

- Make the session open again (allowing up to MAX number of people in)
- Return to start:
If player array is empty at timeout,
End session
Else if Number of Players is 1,
Establish / Instantiate AI
*/
Table t;
View v;

static void detectResize(int sig);
void printResize(void);
void betPhase();
std::string drawBalances();
int firstBetter;
int playersCount;
int foldCount;

int main(int argc, char* argv[])
{
	std::srand((unsigned int)time(NULL));

	// initialize view and draw the main menu
	v.init();
	v.drawMainMenu();
	
	// add human player to table and set ID
	t.addPlayer(true);
	t.getPlayer(0)->setID(0);

	//Fill in empty seats with agents
	for (int i = 1; i < 8; i++) 
	{
		t.addPlayer(false);
		t.getPlayer(i)->setID(i);
	}

	while (t.getSeats()>1){

		t.dealCards(); //deals cards to players

		v.drawTopBanner(drawBalances());
		v.drawAllHands(t, true, true);

		t.getPlayer((t.getDealer() + 2) % t.getSeats())->bet(t.getBigBlind());
		t.getPlayer((t.getDealer() + 1) % t.getSeats())->bet(t.getBigBlind() / 2);

		t.setAmount(t.getBigBlind());

		v.drawTopBanner(drawBalances());

		playersCount = t.getSeats();
		foldCount = t.getSeats();

		firstBetter = ((t.getDealer() + 3) % t.getSeats());
		betPhase();
		firstBetter = ((t.getDealer() + 1) % t.getSeats());

		t.burn();
		t.drawToTable();

		if (foldCount > 1){
			v.drawCommunityCards(t.getCardsOnTable());
			sleep(1);
		}

		t.drawToTable();

		if (foldCount > 1){
			v.drawCommunityCards(t.getCardsOnTable());
			sleep(1);
		}

		t.drawToTable();

		if (foldCount > 1){
			v.drawCommunityCards(t.getCardsOnTable());
		}

		if (playersCount > 1) betPhase();

		t.burn();
		t.drawToTable();
		if (foldCount > 1){
			v.drawCommunityCards(t.getCardsOnTable());
		}

		if (playersCount > 1) betPhase(); 

		t.burn();
		t.drawToTable();
		if (foldCount > 1){
			v.drawCommunityCards(t.getCardsOnTable());
		}

		if (playersCount > 1) betPhase();

		if (foldCount > 1){
			v.drawCommunityCards(t.getCardsOnTable());
			v.drawAllHands(t, false, false);
		}

		std::vector<int> winVec = t.winMacro();//Determines winner(s) and distributes winnings
		
		v.resetMessages();
		v.clearMessages();

		for (unsigned int i = 0; i < winVec.size(); i++){
			std::vector<int> winPot;
			if (winVec[i] > 10000){
				winPot.push_back(winVec[i] % 10);
				winPot.push_back((winVec[i] / 10) % 10);
				winPot.push_back((winVec[i] / 100) % 10);
				winPot.push_back((winVec[i] / 1000) % 10);
			}
			else if (winVec[i] > 1000){
				winPot.push_back(winVec[i] % 10);
				winPot.push_back((winVec[i] / 10) % 10);
				winPot.push_back((winVec[i] / 100) % 10);
			}
			else if (winVec[i] > 100){
				winPot.push_back(winVec[i] % 10);
				winPot.push_back((winVec[i] / 10) % 10);
			}
			else{
				winPot.push_back(winVec[i] % 10);
			}
			std::stringstream winners;
			winners << "Winners for Pot " << i << " are: ";
			if (winPot.size() == 4){
				winners << "P" << t.getPlayer(winPot[0])->getID() << ", ";
				winners << "P" << t.getPlayer(winPot[1])->getID() << ", ";
				winners << "P" << t.getPlayer(winPot[2])->getID() << ", ";
				winners << "P" << t.getPlayer(winPot[3])->getID();
			}
			else if (winPot.size() == 3){
				winners << "P" << t.getPlayer(winPot[0])->getID() << ", ";
				winners << "P" << t.getPlayer(winPot[1])->getID() << ", ";
				winners << "P" << t.getPlayer(winPot[2])->getID();
			}
			else if (winPot.size() == 2){
				winners << "P" << t.getPlayer(winPot[0])->getID() << ", ";
				winners << "P" << t.getPlayer(winPot[1])->getID();
			}
			else{
				winners.str("");
				winners << "Winner for Pot " << i << " is: ";
				winners << "P" << t.getPlayer(winPot[0])->getID();
			}
			v.updateMessage(winners.str());
			v.updateMessage("");
			v.printMessages();
		}
		v.drawBottomBanner("Press a Key to Start a New Round");
		v.promptLoop();
		v.resetMessages();
		v.clearScreen();

		////Resets the deck and sets up a new round
		if(t.newRound(0) == 1){
			v.drawTopBanner("You Have Been Eliminated Exit Press a Key to Exit");
			v.promptLoop();
			return 0;
		}
		v.drawBalance(t.getPlayer(0)->getWallet());
	}
}

/*--------------------------------
	Helper Functions
--------------------------------*/

void betPhase()
{
	//Sets the correct betting order based on the "dealer"
	int currentPlayer = firstBetter;
	int lastPlayer = t.getSeats() + currentPlayer - 1;

	//Basically a loop to check if everyone has met the bet
	//i.e there are no outstanding bets
	while(currentPlayer <= lastPlayer)
	{
		//If everyone has folded during this betting phase break the loop
		if(foldCount == 0) break;

		//A conversion from an unbounded frame to a bounded frame
		int currentIndex = currentPlayer % t.getSeats();

		//A check to make sure the player in question is eligible to bet
		if(!(t.getPlayer(currentIndex)->hasFolded()) && (t.getPlayer(currentIndex)->getWallet() != 0)){
			//Start of the AI betting logic a compressed for of player logic
			if(t.getPlayer(currentIndex)->isAI()){
				usleep(500000);
				//If the human player has foled "speed up" the drawing process
				if(!t.getPlayer(0)->hasFolded()) sleep(rand() % 3);
				//A temporary value to see how much the AI has raised
				int walletdiff = t.getPlayer(currentIndex)->getWallet();
				//Tell the AI to make their move and stores the return value which is an encryption
				//of what the AI has said and done
				int AIAll = ((AI*) t.getPlayer(currentIndex))->makeMove(t.getAmount());
				//Sifts out the first digit of the return value which is the AI's move
				int AIMove = AIAll % 10;
				//Sifts out the second digit of the return value which is the AI's flavor message
				int AISay = AIAll / 10;
				//If the AI has raised the current bet
				if(AIMove > 3){
					//Update the betting loop to indicate a player has bet
					lastPlayer = (currentIndex + t.getSeats() - 2);
					currentPlayer = currentIndex;

					int temp = t.getAmount();
					//If the player has effectively gone All-In reduce the total player's eligible
					//for betting
					if(t.getPlayer(currentIndex)->getWallet() == 0) playersCount--;
					//Update the table's current bet amount
					t.setAmount(temp+walletdiff-t.getPlayer(currentIndex)->getWallet());
				}
				//If the AI has folded
				if(AIMove == 1){
					//Remove his displayed hand for clarity
					v.clearHand(t.getPlayer(currentIndex)->getID());
					t.getPlayer(currentIndex)->setFolded(true);
					//Update the player and fold counts
					playersCount--;
					foldCount--;
				}

				//Retrive the text that the AI has "said" and display it
				std::stringstream AIMessages;
				std::vector<std::string> messageVector;
				messageVector = ((AI*) t.getPlayer(currentIndex))->getMessage(AIAll, walletdiff);
				for(int i = 0; i < messageVector.size(); i++){
					v.updateMessage(messageVector[i]);
					v.printMessages();
					//std::cout << messageVector[i] << "\n";
				}
			}

			//Begin the human betting logic
			else{
				int option;
				//Checks to see the avalible options based on bet amounts, etc. and
				//indicate it to the veiw

				//Check, Bet, All-In, Fold
				if (t.getAmount() == t.getPlayer(currentIndex)->getLiveWallet()){
					option = v.promptPlayer(0, t.getAmount());

				}
				//All-In, Fold
				else if (t.getAmount() >= t.getPlayer(currentIndex)->getWallet()){

					option = v.promptPlayer(1, 0);
				}
				//Call, Bet, All-In, Fold
				else{
					option = v.promptPlayer(2, t.getAmount() - t.getPlayer(currentIndex)->getLiveWallet());
				}
				//Input Gathering is done now start the processing of it
				v.clearControls();
				//Fold - takes player out of round
				if (option == -1) {
					t.getPlayer(currentIndex)->setFolded(true);
					v.clearHand(t.getPlayer(currentIndex)->getID());
					playersCount--;
					foldCount--;
				}
				//Check - nothing happens
				else if (option == 0){
					t.getPlayer(currentIndex)->setFolded(false);
				}
				//Call - update players wallets
				else if (option == -2){
					t.getPlayer(currentIndex)->setFolded(false);
					t.getPlayer(currentIndex)->bet(t.getAmount()- t.getPlayer(currentIndex)->getLiveWallet());
				}
				//The user has made a bet
				//The option is the bet amount
				else if (option > 0){
					t.getPlayer(currentIndex)->setFolded(false);
					//Update the final better index
					lastPlayer = (currentIndex + t.getSeats() - 2);
					currentPlayer = currentIndex;
					//Process the Bet
					int temp;
					if(t.getAmount() == t.getPlayer(currentIndex)->getLiveWallet()){
						temp = t.getPlayer(currentIndex)->bet(option);
					}
					else temp = t.getPlayer(currentIndex)->bet(option+t.getAmount());
					t.setAmount(temp);
					//If the players bet was an All-In reduce the eligible players
					if(t.getPlayer(currentIndex)->getWallet() == 0) playersCount--;
				}
				//All-In
				else if (option == -3){
					t.getPlayer(currentIndex)->setFolded(false);
					//Update the final better index
					lastPlayer = (currentIndex + t.getSeats() - 2);
					currentPlayer = currentIndex;
					//Process the Bet
					int playerWallet = t.getPlayer(currentIndex)->getWallet();
					if(playerWallet > t.getAmount()) t.setAmount(playerWallet);
					t.getPlayer(currentIndex)->bet(playerWallet);
					playersCount--;
				}
				else{
					//std::cout << "Please enter a lowercase letter corresponding to your choice..." << std::endl;
				}
				v.drawBalance(t.getPlayer(currentIndex)->getWallet());
			}
		}
		v.drawTopBanner(drawBalances());
		std::vector<int> temp = t.potCalc((lastPlayer + 1) % t.getSeats());
		//while (!temp.empty()){
		//	std::cout << temp[0] << "\n";
		//	temp.erase(temp.begin());
		//}
		v.drawPots(temp);
		currentPlayer++;
	}
}

std::string drawBalances()
{
	std::stringstream balances;
	balances << " Cash: " << t.getPlayer(0)->getWallet();
	for (int i = 1; i < t.getSeats(); i++){
		balances << " ";
		if (t.getPlayer(i)->isAI()) balances << "AI" << t.getPlayer(i)->getID() << ": ";
		else balances << "P" << t.getPlayer(i)->getID() << ": ";
		balances << t.getPlayer(i)->getWallet();
	}
	return balances.str();
}

void detectResize(int sig)
{
	/*v.getDisplay()->handleResize(sig);
	printResize();
	signal(SIGWINCH, detectResize);*/
}

void printResize(void)
{
	//v.reDraw();
}
