#include "../includes/AI.h"

//Lookup Table for the flavor text
//Completely Optional

const std::string movestrings[5] = { "Folded", "Checked", "Called", "Rasied by: ", "gone All-In" };
const std::string saystrings[8] = {	" says:",
								" mumbles somthing",
								"",
								" scratches his head",
								" yells:",
								" twitches a bit",
								" looks stone cold",
								" says:"};
const std::string saystrings2[8] = { " Swiggity Swoogity",
								"",
								"",
								"",
								" YOU FELL INTO MY TRAP CARD",
								"",
								"",
								" Alright I'm throwing down" };

AI::AI(int money): Player(money){}

int AI::makeMove(int roundBet)
{
	int returnVal = 0;
	char option;
	if (roundBet == liveWallet){
		int r = rand() % 100;
		if (r < 1) 
			option = 'a';
		else if (r < 25) 
			option = 'r';
		else
			option = 'c';
	}
	else if (roundBet >= wallet){
		int r = rand() % 100;
		if (r < 10) 
			option = 'a';
		else 
			option = 'f';
	}
	else{
		int r = rand() % 100;
		if (r < 1) 
			option = 'a';
		else if (r < 30) 
			option = 'f';
		else if (r < 34) 
			option = 'r';
		else 
			option = 'k';
	}

	int r = rand() % 100;

	switch (option){
	case 'f':
		//setFolded(true);

		if (r < 5) returnVal = 1;
		else if (r < 50) returnVal = 2;
		else returnVal = 3;

		returnVal = returnVal * 10 + 1;
		return returnVal;

	case 'c':
		//setFolded(false);

		if (r < 5) returnVal = 1;
		else if (r < 30) returnVal = 4;
		else returnVal = 3;

		returnVal = returnVal * 10 + 2;
		return returnVal;

	case 'k':
		bet(roundBet-getLiveWallet());
		//setFolded(false);

		if (r < 5) returnVal = 1;
		else if (r < 25) returnVal = 2;
		else if (r < 50) returnVal = 5;
		else returnVal = 3;

		returnVal = returnVal * 10 + 3;
		return returnVal;

	case 'r':
	{
		double bett;
		bett = rand() % 100 + 1;
		bett = bett * (wallet - roundBet) / 200;
		bett += roundBet;
		int bettt = bett;
		bettt /= 10;
		bettt *= 10;
		bet(bettt);

		if (r < 5) returnVal = 1;
		else if (r < 30) returnVal = 6;
		else if (r < 35) returnVal = 7;
		else returnVal = 3;

		returnVal = returnVal * 10 + 4;
		return returnVal;
	}
	case 'a':
	{
		bet(wallet);

		if (r < 5) returnVal = 1;
		else if (r < 30) returnVal = 2;
		else if (r < 60) returnVal = 8;
		else if (r < 75) returnVal = 4;
		else returnVal = 7;

		returnVal = returnVal * 10 + 5;
		return returnVal;
	}
	default:
		returnVal = 0;
		return returnVal;
	}
}

std::vector<std::string> AI::getMessage(int i, int walletdiff){
	std::vector<std::string> outstrings;
	std::stringstream tempstring;
	int AIMove = i % 10;
	int AISay = i / 10;
	tempstring << "AI " << getID() << " has ";
	tempstring << movestrings[AIMove - 1];
	if (AIMove == 4) tempstring << walletdiff - getWallet();
	outstrings.push_back(tempstring.str());
	tempstring.str("");
	if (AISay == 3) return outstrings;
	tempstring << "AI " << getID() << saystrings[AISay - 1];
	outstrings.push_back(tempstring.str());
	if (AISay == 1 || AISay == 8 || AISay == 5){
		outstrings.push_back(saystrings2[AISay - 1]);
	}
	return outstrings;
}
