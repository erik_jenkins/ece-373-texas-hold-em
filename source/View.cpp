#include "../includes/View.h"

View::View() {
	Display d;
}

Display* View::getDisplay(){
	return &d;
}

int View::getCols() { return d.getCols(); }
int View::getLines() { return d.getLines(); }

std::vector<int> View::getMousePosition() {
	std::vector<int> positionVector;

	positionVector.push_back(d.getMouseEventX());
	positionVector.push_back(d.getMouseEventY());

	return positionVector;
}

int View::getInput() {
	// return -1 if no button pressed
	// 		   0 if DecrementBetButton pressed
	// 		   1 if BetButton pressed
	// 		   2 if IncrementBetButton pressed
	// 		   3 if FoldButton pressed

	int button = clickedButton();
	while (button == -1) {
		drawTopBanner("Looking for input...");
		button = clickedButton();
		if (button != -1)
			return button;
	}

	return -1;
}

void View::drawAllButton() {
	drawButton(getCols() - 14, getLines() - 9, "All-In");
	updateScreen();
}

void View::drawBetButton() {
	drawButton(getCols() - 21, getLines() - 9, "Raise");
	updateScreen();
}

void View::drawBetWindow(int bet){
	stringstream betAmountString;
	betAmountString << bet;
	d.drawBox(getCols() - 21, getLines() - 6, 7, 3, 0);
	drawText(getCols() - 20, getLines() - 5, betAmountString.str());
	d.updateScreen();
	updateScreen();
}

void View::drawIncrementBetButton() {
	drawButton(getCols() - 14, getLines() - 6, ">");
	drawButton(getCols() - 11, getLines() - 6, ">>");
	drawButton(getCols() - 7, getLines() - 6, ">>>");
	updateScreen();
}

void View::drawDecrementBetButton() {
	drawButton(getCols() - 25, getLines() - 6, "<");
	updateScreen();
}

void View::drawCallButton() {
	drawButton(getCols() - 21, getLines() - 9, "Call ");
	updateScreen();
}

void View::drawCheckButton() {
	drawButton(getCols() - 21, getLines() - 3, "Check");
	updateScreen();
}

void View::drawFoldButton() {
	drawButton(getCols() - 14, getLines() - 3, "Fold");
	updateScreen();
}

//Draw the Betting Suite
void View::drawBetting(int in) {
	drawCallButton();
	drawBetWindow(in);
	drawIncrementBetButton();
	drawDecrementBetButton();
	drawAllButton();
}

void View::clearBetWindow() {
	d.eraseBox(getCols() - 21, getLines() - 6, 7, 3);
	updateScreen();
}

void View::clearControls(){
	d.eraseBox(getCols() - 25, getLines() - 9, getCols() - 1, getLines() - 1);
}

/*------------------------------
	End of Betting Buttons
	------------------------------*/

void View::drawMainMenu() {
	stringstream messageString;

	messageString << " Welcome to Texas Hold 'Em! Press a key to Start";
	d.bannerTop(messageString.str());
	d.updateScreen();
	promptLoop();
	stringstream start;
	start << " Starting Game";
	d.bannerTop(start.str());
	d.updateScreen();
	sleep(1);
	clearScreen();
}

/*void View::drawCard(Card card, int x, int y) {
	d.DisplayCard(x, y, card.getSuit(), card.getRank(), 0);
	d.updateScreen();
	}*/

void View::drawCard(int card, int x, int y) {
	if (card == -1) d.DisplayCard(x, y, 4, 2, 0);
	else d.DisplayCard(x, y, card / 13, (card % 13) + 2, 0);
	d.updateScreen();
}

/*void View::drawCommunityCards(std::vector<Card> communityCards) {
	communityCardBuffer = communityCards;
	for(int i = 0; i < communityCards.size(); i++) {
	Card card = communityCards[i];
	drawCard(card, 8*i+d.getCols()/2-17, d.getLines()/2-3);
	}

	d.updateScreen();
	}*/

void View::drawCommunityCards(std::vector<int> communityCards) {
	for (int i = 0; i < communityCards.size(); i++) {
		int card = communityCards[i];
		drawCard(card, 8 * i + d.getCols() / 2 - 17, d.getLines() / 2 - 3);
	}

	d.updateScreen();
}

/*void View::drawHand(Hand hand) {
	handBuffer = hand;
	for(int i = 0; i < 2; i++) {
	Card card = hand.getCards()[i];
	drawCard(card, 4*i+d.getCols()/2-3, d.getLines()-8);
	}

	d.updateScreen();
	}

	/*void View::drawAllHands(Table t, bool delay, bool backs){
	for(int j = 0; j < 2; j++){
	for(int i = 0; i < t.getSeats(); i++){
	if(!(t.getPlayer(i)->hasFolded())){
	Card card = t.getPlayer(i)->getHand().getCards()[j];
	if(i == 0){
	drawCard(card, 4*j+getCols()/2-3, getLines()-8);
	}
	else if(i < 7){
	if(backs) card = Card(Card::BACK,Card::ACE);
	drawCard(card, 4*j+getCols()/(7)*i+3, 2);
	}
	else if(i == 7){
	if(backs) card = Card(Card::BACK,Card::ACE);
	drawCard(card, 4*j+getCols()/(7)*6+3, 8);
	}

	if(delay) usleep(200000);
	}
	}
	}
	}*/

void View::drawAllHands(std::vector<int> t, bool delay, bool backs, int ID){
	for (int i = 0; i < t.size(); i++){
		int card = t[i] % 100;
		int id = t[i] / 100;
		id = (id - ID + 8) % 8;
		int j = i / (t.size() / 2);
		if (id == 0){
			drawCard(card, 4 * j + getCols() / 2 - 3, getLines() - 8);
		}
		else if (id < 7){
			if (backs) card = -1;
			drawCard(card, 4 * j + getCols() / 7*id + 3, 2);
		}
		else if (id == 7){
			if (backs) card = -1;
			drawCard(card, 4 * j + getCols() / (7) * 6 + 3, 8);
		}
		if (delay) usleep(200000);
	}
}

void View::drawBalance(int balance){
	balanceBuffer = balance;
	d.eraseBox(0, 0, 14, 1);
	std::stringstream bal;
	bal << "Cash: " << balance;
	drawText(0, 0, bal.str());
	d.updateScreen();
}

void View::drawText(int x, int y, std::string text) {
	d.drawText(x, y, text.c_str());
}

void View::drawPots(std::vector<int> pots) {
	d.eraseBox(0, 1, 15, 1);
	d.eraseBox(0, 2, 14, 5);
	std::stringstream outputString;

	for (int i = 0; i < pots.size(); i++) {
		outputString.str("");
		if (i == 0) {
			outputString << "Main Pot: " \
				<< pots.at(i);
			drawText(0, 1, outputString.str());
		}
		else {
			outputString << "Pot# " << i \
				<< ": " << pots.at(i);
			drawText(0, 1 + i, outputString.str());
		}
	}

	d.updateScreen();
}

void View::drawButton(int x, int y, std::string text) {
	d.drawBox(x, y, text.length() + 2, 3, 0);
	drawText(x + 1, y + 1, text);
	d.updateScreen();
}

void View::drawTopBanner(std::string text) {
	d.bannerTop(text);
	updateScreen();
}

void View::drawBottomBanner(std::string text) {
	d.bannerBottom(text);
	updateScreen();
}

int View::promptPlayer(int prompt, int raise){
	betAmount = raise;
	viewState = prompt;
	stringstream message;
	int raisePrompt = raise;
	int keyPress = 0;
	int eventButton = -1;
	switch (prompt){
	case 0:
		raisePrompt = 0;
		drawCheckButton();
		drawBetting(-raisePrompt);
		drawFoldButton();
		drawBetButton();
		while (1){
			keyPress = d.captureInput();
			if (keyPress < 0){
				eventButton = d.getMouseEventButton();
				if (eventButton == 4){
					int cb = clickedButton();
					if (cb == 6) return 0;
					else if (cb == 0){
						raisePrompt = 0;
						clearBetWindow();
						drawBetting(-raisePrompt);
						drawBetButton();
					}
					else if (cb == 1){
						return (-raisePrompt);
					}
					else if (cb == 2){
						if (-raisePrompt + 10 < raise) raisePrompt = -raise;
						else raisePrompt -= 10;
						clearBetWindow();
						drawBetting(-raisePrompt);
						drawBetButton();
					}
					else if (cb == 3){
						if (-raisePrompt + 100 < raise) raisePrompt = -raise;
						else raisePrompt -= 100;
						clearBetWindow();
						drawBetting(-raisePrompt);
						drawBetButton();
					}
					else if (cb == 4){
						if (-raisePrompt + 1000 < raise) raisePrompt = -raise;
						else raisePrompt -= 1000;
						clearBetWindow();
						drawBetting(-raisePrompt);
						drawBetButton();
					}
					else if (cb == 5) return -1;
					else if (cb == 7) return -3;
				}
			}
		}
		break;
	case 1:
		drawAllButton();
		drawFoldButton();
		while (1){
			keyPress = d.captureInput();
			if (keyPress < 0){
				eventButton = d.getMouseEventButton();
				if (eventButton == 4){
					int cb = clickedButton();
					if (cb == 7) return -3;
					else if (cb == 5) return -1;
				}
			}
		}
		break;
	case 2:
		drawBetting(raise);
		drawFoldButton();
		while (1){
			keyPress = d.captureInput();
			if (keyPress < 0){
				eventButton = d.getMouseEventButton();
				if (eventButton == 4){
					int cb = clickedButton();
					if (cb == 0){
						raisePrompt = raise;
						clearBetWindow();
						drawBetting(raisePrompt);
					}
					else if (cb == 1){
						if (raisePrompt == raise) return -2;
						return ((-raisePrompt) - raise);
					}
					else if (cb == 2){
						if (raisePrompt > 0) raisePrompt = -10;
						else raisePrompt -= 10;
						clearBetWindow();
						drawBetting(-raisePrompt);
						drawBetButton();
					}
					else if (cb == 3){
						if (raisePrompt > 0) raisePrompt = -100;
						else raisePrompt -= 100;
						clearBetWindow();
						drawBetting(-raisePrompt);
						drawBetButton();
					}
					else if (cb == 4){
						if (raisePrompt > 0) raisePrompt = -1000;
						else raisePrompt -= 1000;
						clearBetWindow();
						drawBetting(-raisePrompt);
						drawBetButton();
					}
					else if (cb == 5) return -1;
					else if (cb == 7) return -3;
				}
			}
		}
		break;
	default:
		clearControls();
		break;
	}
	return -10;
}

void View::updateMessage(std::string message){
	if (messageBlock.size() < 9){
		messageBlock.push_back(message);
	}
	else{
		messageBlock.erase(messageBlock.begin());
		messageBlock.push_back(message);
	}
}

void View::resetMessages(){
	while (!messageBlock.empty()){
		messageBlock.pop_back();
	}
}

void View::printMessages(){
	clearMessages();
	for (int i = 0; i < messageBlock.size(); i++){
		drawText(0, getLines() - i - 1, messageBlock[messageBlock.size() - i - 1]);
	}
	updateScreen();
}

void View::clearMessages(){
	d.eraseBox(0, getLines() - 9, 28, 10);
	updateScreen();
}

void View::setState(int state){
	viewState = state;
}

int View::getState(){
	return viewState;
}

void View::init(){
	d.init();
}

void View::clearScreen(void) {
	d.eraseBox(0, 0, d.getCols(), \
		d.getLines());
	d.updateScreen();
}

void View::clearCommunityCards(void) {
	d.eraseBox(d.getCols() / 2 - 17, d.getLines() / 2 - 3, 38, 5);
	d.updateScreen();
}

void View::clearHand(int player){
	if (player == 0){
		d.eraseBox(getCols() / 2 - 3, getLines() - 8, 10, 5);
	}
	else if (player < 7){
		d.eraseBox(getCols() / (7)*player + 3, 2, 10, 5);
	}
	else if (player == 7){
		d.eraseBox(getCols() / (7) * 6 + 3, 8, 10, 5);
	}
}

void View::promptLoop(){
	int keyPress = 0;
	while (1){
		keyPress = d.captureInput();
		if (keyPress > 0) break;
	}
}

void View::reDraw(){
	clearScreen();
	printMessages();
	//drawHand(handBuffer);
	//drawCommunityCards(communityCardBuffer);
	promptPlayer(viewState, betAmount);
	drawBalance(balanceBuffer);
	updateScreen();
}

void View::handleResize(int sig) {
	d.handleResize(sig);
}

void View::updateScreen(void) {
	d.updateScreen();
}

/*------------------------------
		Helper Methods
		------------------------------*/

int View::clickedButton() {
	// return -1 if no button pressed
	// 		   0 if DecrementBetButton pressed
	// 		   1 if Bet/CallButton pressed
	// 		   2 if IncrementBetButton1 pressed
	// 		   3 if IncrementBetButton2 pressed
	// 		   4 if IncrementBetButton3 pressed
	// 		   5 if FoldButton pressed
	// 		   6 if CheckButton pressed
	// 		   7 if All-InButton pressed

	// Decrement
	if ((d.getMouseEventX() > getCols() - 26 && d.getMouseEventX() < getCols() - 22) \
		&& (d.getMouseEventY() > getLines() - 7 && d.getMouseEventY() < getLines() - 3))
		return 0;
	// Bet/Call
	if ((d.getMouseEventX() > getCols() - 22 && d.getMouseEventX() < getCols() - 14) \
		&& (d.getMouseEventY() > getLines() - 10 && d.getMouseEventY() < getLines() - 5))
		return 1;
	// Increment1
	if ((d.getMouseEventX() > getCols() - 15 && d.getMouseEventX() < getCols() - 11) \
		&& (d.getMouseEventY() > getLines() - 8 && d.getMouseEventY() < getLines() - 3))
		return 2;
	// Increment2
	if ((d.getMouseEventX() > getCols() - 12 && d.getMouseEventX() < getCols() - 7) \
		&& (d.getMouseEventY() > getLines() - 7 && d.getMouseEventY() < getLines() - 3))
		return 3;
	// Increment3
	if ((d.getMouseEventX() > getCols() - 8 && d.getMouseEventX() < getCols() - 2) \
		&& (d.getMouseEventY() > getLines() - 7 && d.getMouseEventY() < getLines() - 3))
		return 4;
	// Fold
	if ((d.getMouseEventX() > getCols() - 15 && d.getMouseEventX() < getCols() - 8) \
		&& (d.getMouseEventY() > getLines() - 4 && d.getMouseEventY() < getLines() + 1))
		return 5;
	// Check
	if ((d.getMouseEventX() > getCols() - 22 && d.getMouseEventX() < getCols() - 14) \
		&& (d.getMouseEventY() > getLines() - 4 && d.getMouseEventY() < getLines() + 1))
		return 6;
	// All-In
	if ((d.getMouseEventX() > getCols() - 15 && d.getMouseEventX() < getCols() - 6) \
		&& (d.getMouseEventY() > getLines() - 10 && d.getMouseEventY() < getLines() - 5))
		return 7;
	// None
	return -1;
}
