#include "../includes/Table.h"


const int blindTable[24] = { 50, 100, 200, 300, 400, 600, 800, 1000, \
1200, 2000, 3000, 4000, 6000, 8000, 10000, 12000, 16000, 20000 };

Table::Table()
{
	roundInterval = 3;
	initialMoney = 10000;
	maxPlayers = 8;
	betAmount = 0;
	roundNumber = 0;
	dealer = 0;
	pot.resize(maxPlayers, (std::vector<int>(maxPlayers + 1)));
	deck.shuffle();
}

Table::Table(int money, int players, int speed)
{
	roundInterval = speed;
	initialMoney = money;
	maxPlayers = players;
	dealer = 0;
	betAmount = 0;
	roundNumber = 0;
	pot.resize(maxPlayers, (std::vector<int>(maxPlayers+1)));
	deck.shuffle();
}

/*--------------------------------
	Basic Setters/Getters
--------------------------------*/

void Table::setAmount(int amt)
{
	betAmount = amt;
}

void Table::setPot(int size, int potnum)
{
	pot[potnum][0] = size;
}

void Table::setRound(int r)
{
	roundNumber = r;
}

int Table::getAmount()
{
	return betAmount;
}

int Table::getPot(int potnum)
{
	return pot[potnum][0];
}

int Table::getRound()
{
	return roundNumber;
}

int Table::getSeats()
{
	return (int) seats.size();
}

Player* Table::getPlayer(int seatIndex)
{
	return &seats[seatIndex];
}

void Table::drawToTable()
{
	cardsOnTable.push_back(deck.draw());
}

void Table::addCardToTable(Card c)
{
	cardsOnTable.push_back(c);
}

std::vector<Card> Table::getCardsOnTable(){
	return cardsOnTable;
}

void Table::dealCards()
{
	unsigned int i = 0;
	deck.shuffle();
	while (i < seats.size()){
		seats[i].addCard(deck.draw());
		seats[i].addCard(deck.draw());
		i++;
	}
}

/*--------------------------------
	New Get/Sets (Update 2)
--------------------------------*/

int Table::setPlayer(Player newPlayer)
{
	if (seats.size() < (unsigned int) maxPlayers + 1){
		seats.push_back(newPlayer);
		return 0;
	}
	return -1;
}

int Table::getBigBlind()
{
	if (roundNumber/roundInterval > 23){
		return(blindTable[23] * initialMoney / 10000);
	}
	return (blindTable[roundNumber/roundInterval] * initialMoney / 10000);
}

/*--------------------------------
	New Get/Sets (Update 3)
--------------------------------*/

int Table::removePlayer(int seatIndex)
{
	if(seatIndex >= maxPlayers) return -1;
	if (dealer > seatIndex) dealer--;
	seats.erase(seats.begin()+seatIndex);
	return 0;
}

void Table::burn()
{
	deck.draw();
}

/*--------------------------------
	Essential State Flow Functions
--------------------------------*/

void Table::winPlayer(int seatIndex, int winAmt)
{
	seats[seatIndex].increaseWallet(winAmt);
}

int Table::getWinner(int potnum)
{
	int high[8] = {-1 , -1, -1, -1, -1 , -1, -1, -1};
	int highIndex = 0;
	int highRank = -1;
	for (int i = 1; i < getSeats() + 2; i++ ){
		if (pot[potnum][i] == 1){
			if (highRank == seats[i - 1].getHandRank()){
				highIndex++;
				high[highIndex] = i - 1;
			}
			else if (highRank < seats[i - 1].getHandRank()){
				highRank = seats[i - 1].getHandRank();
				highIndex = 0;
				for (int j = 0; j < 8; j++){
					high[j] = -1;
				}
				high[highIndex] = i - 1;
			}
		}
	}
	if (highIndex > 0){
		for (int j = 0; j < 5; j++){
			int highVal = 0;
			for (int i = 0; i < 8; i++){
				if (high[i] > -1){
					int temp = seats[high[i]].getTieBreaker()[seats[high[i]].getTieBreaker().size() - j - 1];
					if (temp > highVal){
						for (int k = (i - 1); k > -1; k--){
							high[k] = -1;
						}
						highVal = temp;
					}
				}
			}
		}
		int redex = 1;
		for (int i = 0; i < 8; i++){
			if (high[i] > -1){
				redex = redex * 10 + high[i];
			}
		}
		return redex;
	}
	return(high[highIndex]);
}

void Table::potDistribute(){
	int min;
	for (int j = 0; j < maxPlayers; j++){
		min = 999999999;
		for (int i = 0; i < getSeats(); i++){
			if ((min > seats[i].getLiveWallet()) && !seats[i].hasFolded()){
				min = seats[i].getLiveWallet();
			}
		}
		if (min != 999999999){
			for (int i = 0; i < getSeats(); i++){
				if (seats[i].getLiveWallet() > 0){
					if (seats[i].getLiveWallet() >= min){
						seats[i].setLiveWallet(seats[i].getLiveWallet()-min);
						pot[j][0] += min;
					}
					else{
						pot[j][0] += seats[i].getLiveWallet();
						seats[i].setLiveWallet(0);
					}
					if(!seats[i].hasFolded()) pot[j][i+1] = 1;
				}
			}
		}
	}
}

//Takes in an  int to represet the highest better
std::vector<int> Table::potCalc(int highDex){
	int highest = 0;
	std::vector<int> repot;
	repot.push_back(0);
	int min;
	for (int j = 0; j < maxPlayers; j++){
		min = 999999999;
		for (int i = 0; i < getSeats(); i++){
			if ((min > (seats[i].getLiveWallet() - highest))
				&& !seats[i].hasFolded()
				&& ((seats[i].getWallet() == 0) || (i == highDex))
				&& (seats[i].getLiveWallet() - highest) > 0){
				min = seats[i].getLiveWallet();
			}
		}
		if (min != 999999999){
			repot.push_back(0);
			for (int i = 0; i < getSeats(); i++){
				if (seats[i].getLiveWallet() > highest){
					if(seats[i].getLiveWallet() <= (min - highest)){
						repot[j] += seats[i].getLiveWallet() - highest;
					}
					else repot[j] += min - highest;
				}
			}
		}
		highest = min;
	}
	return repot;
}


/*--------------------------------
	Non-Essential Macros (Update 2)
--------------------------------*/

std::vector<int> Table::winMacro()
{
	potDistribute();

	for (int i = 0; i < getSeats(); i++){
		seats[i].updateBuffer(cardsOnTable);
		seats[i].calcHandRank();
	}

	std::vector<int> allWinners;
	int i = 0;
	while (pot[i][0] > 0){
		int winners = getWinner(i);
		allWinners.push_back(winners);
		if (winners > 10000){
			winPlayer(winners % 10, pot[i][0] / 4);
			winPlayer((winners / 10) % 10, pot[i][0] / 4);
			winPlayer((winners / 100) % 10, pot[i][0] / 4);
			winPlayer((winners / 1000) % 10, pot[i][0] / 4);
		}
		else if (winners > 1000){
			winPlayer(winners % 10, pot[i][0] / 3);
			winPlayer((winners / 10) % 10, pot[i][0] / 3);
			winPlayer((winners / 100) % 10, pot[i][0] / 3);
		}
		else if (winners > 100){
			winPlayer(winners % 10, pot[i][0] / 2);
			winPlayer((winners / 10) % 10, pot[i][0] / 2);
		}
		else{
			winPlayer(winners % 10, pot[i][0]);
		}
		i++;
	}
	return allWinners;
}
/*

decoder(std::vector<int> in){
	for(int i = 0; i < in.size(); i++){
		if (in[i] > 10000){
			winner1 = in[i] % 10;
			winner2 = (in[i] / 10) % 10;
			winner3 = (in[i] / 100) % 10;
			winner4 - (in[i] / 1000) % 10;
		}
		else if (in[i] > 1000){
			winner1 = in[i] % 10;
			winner2 = (in[i] / 10) % 10;
			winner3 = (in[i] / 100) % 10;
		}
		else if (in[i] > 100){
			winner1 = in[i] % 10;
			winner2 = (in[i] / 10) % 10;
		}
		else{
			winner1 = in[i] % 10;
			}

*/

int Table::newRound(int playerID)
{
	roundNumber++;
	for (int i = 0; i < maxPlayers; i++){
		for (int j = 0; j < maxPlayers + 1; j++){
			pot[i][j] = 0;
		}
	}
	for (int i = 0; i < 5; i++){
		cardsOnTable.pop_back();
	}
	for (unsigned int i = 0; i < seats.size(); i++){
		seats[i].newRound();
		seats[i].clearHand();
	}

	for (unsigned int i= 0; i < seats.size(); i++){
		if(seats[i].getWallet() == 0){
			if(seats[i].getID() == playerID){
				seats.erase(seats.begin() + i);
				return 1;
			}
		seats.erase(seats.begin() + i);
		}
	}

	deck = Deck();
	deck.shuffle();
	dealer++;
	dealer = dealer % getSeats();
	return 0;
}

int Table::addPlayer(bool player)
{
	if(player){
		Player newPlayer(initialMoney);
		return (setPlayer(newPlayer));
	}
	else {
		AI newPlayer(initialMoney);
		newPlayer.setAI();
		return (setPlayer(newPlayer));
	}
	
}

int Table::getVaild(int seat, int potnum){
	return (pot[potnum][seat+1]);
}

void Table::setVaild(int seat, int potnum, int vnv){
	pot[potnum][seat+1] = vnv;
}

int Table::getDealer(){
	return dealer;
}

/*--------------------------------
	State Restore Functions
--------------------------------*/

