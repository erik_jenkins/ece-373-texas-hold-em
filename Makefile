CC = g++
DEBUG = 
DEPS = ./includes/
ODIR = ./bin/
DRIVE = ./drivers/
CFLAGS = -c -Wall $(DEBUG) -pthread -lpthread
# For Ubuntu
LDFLAGSU = -lncursesw $(DEBUG)
# For Mac
#LDFLAGSM = -lncurses

OBJ = $(patsubst ./source/%.cpp, $(ODIR)%.o, $(wildcard ./source/*.cpp))
FILE = $(patsubst ./source/%.cpp, %, $(wildcard ./source/*.cpp))

allU: TexasHoldEmU

TexasHoldEmU: $(OBJ)
	$(CC) -o ./bin/TexasHoldEm $^ $(LDFLAGSU)

allM: TexasHoldEmM

TexasHoldEmM: $(OBJ)
	$(CC) -o ./bin/TexasHoldEm $^ $(LDFLAGSM)

$(ODIR)%.o: ./source/%.cpp $(DEPS)%.h
	$(CC) -c $< -o $@

%Test: $(DRIVE)*/%Driver.cpp $(DEPS)%.h
	$(CC) $< -o ./bin/$@

clean:
	rm -f $(ODIR)*.o
	rm -f $(ODIR)*Test
	rm -f TexasHoldEm
