#pragma once

#include "Display.h"
//#include "Card.h"
//#include "Hand.h"
//#include "Player.h"
//#include "Table.h"
#include <vector>
#include <string>

#include <sstream>
#include <cstdlib>
#include <signal.h>

class View {
public:
	View();

	Display* getDisplay(void);
	void init();

	std::vector<int> getMousePosition();
	int getInput(void);

	int getCols(void);
	int getLines(void);

	//Betting Button Macros
	void drawAllButton(void);
	void drawBetButton(void);
	void drawBetWindow(int bet);
	void drawIncrementBetButton(void);
	void drawDecrementBetButton(void);
	void drawCallButton(void);
	void drawCheckButton(void);
	void drawFoldButton(void);
	//Draw the betting suite macro
	void drawBetting(int in);

	//Clears the raise numbers displayed
	void clearBetWindow(void);
	//Clears the betting suite when turn is over
	void clearControls(void);
	//End of Betting Button Methods

	void drawMouseEvent(void);

	void drawMainMenu(void);
	void drawBalance(int balance);
	//void drawCard(Card card, int x, int y);
	void drawCard(int card, int x, int y);
	//void drawCommunityCards(std::vector<Card> communityCards);
	void drawCommunityCards(vector<int> communityCards);
	//void drawHand(Hand hand);
	//void drawAllHands(Table t, bool delay, bool backs);
	void drawAllHands(vector<int> t, bool delay, bool backs, int ID);
	void drawText(int x, int y, std::string text);
	void drawPots(std::vector<int> pots);
	void drawButton(int x, int y, std::string text);
	void drawTopBanner(std::string text);
	void drawBottomBanner(std::string text);

	void clearScreen(void);
	void clearCommunityCards(void);
	void clearMessages(void);
	void clearHand(int player);
	void handleResize(int sig);
	void updateScreen(void);
	int promptPlayer(int prompt, int raise);
	void reDraw();

	void updateMessage(std::string message);
	void resetMessages();
	void printMessages();

	void setState(int state);
	int getState();

	int getBetAmount(void);
	void setBetAmount(int betAmount);

	void promptLoop();

private:

	Display d;
	int betAmount;
	int viewState;
	std::vector<string> messageBlock;
	//std::vector<Card> communityCardBuffer;
	//Hand handBuffer;
	//std::vector<Hand> otherHandBuffer;
	int balanceBuffer;

	//Converts the mouse position to a button clicked
	int clickedButton();
};
