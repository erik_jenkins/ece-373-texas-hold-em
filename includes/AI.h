#pragma once

#include "Player.h"
#include <cstdlib>


class AI : public Player{
public:

	//Flavor Lookup Table
	//enum AISAY { SHREKT = 0, MUMBLE, MUTE, SCRATCH, TRAP, TWITCH, COLD, THROW };

	AI(int money);

	int makeMove(int roundBet);
	std::vector<std::string> getMessage(int i, int walletdiff);

//private:

//	Player::MOVE move;
//	AISAY say;

};
