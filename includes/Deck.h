#pragma once

#include "Card.h"
#include <exception>
#include <vector>
#include <algorithm>
#include <ctime>

class emptyDeckException : public std::exception {
public:
	virtual const char* what() const throw() {
		return "Deck is empty!";
	}
};

class Deck {

public:
	Deck();
	Card draw();
	int count();
	void shuffle();
	Card get(int index);


private:
	std::vector<Card> cards;
};