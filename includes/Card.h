#pragma once

#include <string>
#include <sstream>

class Card {
public:
	enum SUIT { SPADES, CLUBS, DIAMONDS, HEARTS, BACK };
	enum RANK {
		TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, \
		NINE, TEN, JACK, QUEEN, KING, ACE
	};

	//Constructors
	//Card();
	Card(SUIT suit, RANK rank);

	//Set Operators
	void setSuit(SUIT suit);
	void setRank(RANK rank);

	//Get Operators
	Card::SUIT getSuit();
	Card::RANK getRank();

	std::string toString();
	int serialize();
private:
	SUIT suit;
	RANK rank;
};
