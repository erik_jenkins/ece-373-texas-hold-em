#pragma once

#include "Card.h"
#include <vector>
#include <exception>

class emptyHandException : public std::exception {
public:
	virtual const char* what() const throw() {
		return "Hand is empty!";
	}
};

class Hand {
public:
	Hand();
	void add(Card card);
	Card play(int cardInt);
	std::vector<Card> getCards();
	void popCard();
	void clearHand();
	
private:
	std::vector<Card> cards;
};
