#pragma once

#include "Card.h"
#include "Deck.h"
#include "Hand.h"
#include "Player.h"
#include "Table.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

#include "xmlrpc-c/base.hpp"
#include "xmlrpc-c/registry.hpp"
#include "xmlrpc-c/server_abyss.hpp"
