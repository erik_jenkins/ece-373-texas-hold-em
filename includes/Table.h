#pragma once

#include "Player.h"
#include "AI.h"
#include "Deck.h"


class Table
{
public:
	Table();
	Table(int money, int players, int speed);

	void setAmount(int amt);
	int getAmount();

	int getPot(int potnum);
	void setPot(int size, int potnum);

	void setRound(int r);
	int getRound();
	
	int getSeats();
	int getBigBlind();
	int getDealer();

	int setPlayer(Player newPlayer);
	Player* getPlayer(int seatIndex);
	
	
	int addPlayer(bool ai);
	int removePlayer(int seatIndex);

	void potDistribute();
	std::vector<int> potCalc(int highDex);

	std::vector<int> winMacro();
	void winPlayer(int seatIndex, int winAmt);
	int getWinner(int potnum);

	int newRound(int playerID);
	void dealCards();
	void drawToTable();
	void addCardToTable(Card c);
	void burn();
	std::vector<Card> getCardsOnTable();

	int getVaild(int seat, int potnum);
	void setVaild(int seat, int potnum, int vnv);


private:

	//All but pot needs serailizing
	int initialMoney;
	int roundInterval;
	int maxPlayers;
	int betAmount;
	int roundNumber;
	int dealer;
	Deck deck;
	std::vector<Card> cardsOnTable;
	std::vector<Player> seats;
	
	std::vector<std::vector<int> > pot;
	
};

