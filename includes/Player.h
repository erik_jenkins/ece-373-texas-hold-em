#pragma once

#include "Hand.h"
#include <vector>
#include <iostream>

class Player{
	public:
		
		//Define a player moveset for shortcuts and readability
		//enum MOVE { FOLD = 0, CHECK, CALL, RAISE, ALLIN };

		//Constructors
		//Player();
		Player(int money);
		
		int getWallet(); //returns the current amount of money in player's wallet
		void increaseWallet(int pot); //adds input to current wallet

		int getLiveWallet(); //Gets the players "live wallet" the money he/she has played in the round
		void setLiveWallet(int seto);

		int getID(); //Gets the Players static ID
		void setID(int id); //Sets a players static ID

		bool hasFolded(); //Determines if player has folded
		void setFolded(bool f); //Sets the players fold status

		void setAI(); //Determines of the players is an AI
		bool isAI(); //Sets wether or the player is an AI

		Hand getHand(); //Returns the hand of the player
		void addCard(Card card); //Places card objects in player's hand attribute
		void clearHand(); //Clears a players Hand

		int bet(int current); //Takes in current bet and deducts from wallet

		//Algorithm Methods
		//Places cards located in community pile into player buffer
		//for calculations in the winning hand
		void updateBuffer(std::vector<Card> communityCards);
		
		int getHandRank(); //returns int representing hand rank
		void calcHandRank();
		std::vector<int> getTieBreaker();

		void newRound();
		
	protected:
		//Persistent Variables, not cleared per round
		int wallet;
		int ID;
		bool AI_FLAG;

		//Round based variables
		bool Fold_FLAG;
		
		Hand hand;

		int liveWallet; //To keep track of the amount the player has bet for all-in calculations
		int rank; //Rank of hand
		std::vector<Card> communityCardBuffer;

		//Winning Hand Variables
		int suitCount[4]; //Array for the number of a certain suit on table
		int cardCount[14]; //Array for the number of a certain value on table
		int occurrenceCount[5]; //array for number of zeros, singletons, pairs, triples, and quads
		std::vector<int> winHand;

		//Helper Methods
		bool flushCheck();
		bool straightCheck();
		int occurrenceCheck();
		void setRank(int num);
		void setTieBreaker();
		

		//Helpers for tiebreaking cards
		void pairWin();
};
