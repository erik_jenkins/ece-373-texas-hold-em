#pragma once

#include "Card.h"
#include "Deck.h"
#include "Hand.h"
#include "Player.h"
#include "Table.h"
#include "Display.h"
#include "View.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <signal.h>

class TexasHoldEm {
public:
	TexasHoldEm();
	~TexasHoldEm();
};
