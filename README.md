# Texas Hold 'Em! #
A C++ implementation of the classic poker variant; written by Jake Spinney, Willy Young, Matt Perry, and Erik Jenkins

---

**Instructions for setting up git**: 

1. First, clone the current repo to your preferred directory by copying the HTTP address of the repo at the top right of the overview page. 
2. Use the `git clone <address>` command to clone the current repo (along with all the different branches) to your local directory.
3. Create a new branch on which you will implement your responsibilities (e.g. card) using the command `git checkout -b <branch>`.
4. When you have made sufficient progress for a commit, add all modified files to the staging area using `git add <file1> <file2> <etc>...`
5. Commit your changes with the command `git commit -m "<message>"` and push it to Bitbucket with the command `git push origin <branch>`, where `<branch>` is the branch you are working off of and `origin` is the remote for Bitbucket (see `git remote -v`).

**Instructions for running the code**

1. Run the Makefile in the `ece-373-texas-hold-em` folder
2. Run `./bin/TexasHoldEm`
3. ???? (about tree fiddy)
4. Profit

**Instructions for running the tests**

1. There is a folder for each testing setup
2. Run the `<class>Test.sh` script for each folder
3. If the tests were successful, the script should print out:
	"Tests were successful. Yay!"
4. ????
5. Profit some more